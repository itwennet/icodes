package cn.com.codes.framework.common.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import cn.com.codes.framework.common.config.OrderedProperties;
import cn.com.codes.framework.hibernate.HibernateGenericController;

public class MypmBean extends OrderedProperties {

	private static final long serialVersionUID = 1L;

	/** this points to an ordinary file */
	private String contextPath;
    
	private static MypmBean instance = null;
	 
	public  static MypmBean getInstacne(){

		return instance ;
	}
	protected static Log log = LogFactory.getLog(HibernateGenericController.class);


	private MypmBean() {

	}
	
	public MypmBean(String docname) throws IOException {
		contextPath = docname;
		try {
			load(new FileInputStream(contextPath));
		} catch (IOException e) {
			log.error("Unable to read from " + contextPath, e);
			throw e;
		}
	}

	public static void main(String[] args) {



	}

	public static void initConfFile(OutputStream out, Properties conf)
			throws IOException {
		Vector keys = new Vector();

		for (Enumeration e = conf.propertyNames(); e.hasMoreElements();) {
			keys.addElement((String) (e.nextElement()));
		}

		// sort them
		Collections.sort(keys);

		// write the header
		DataOutputStream dataoutputstream = new DataOutputStream(out);
		// dataoutputstream.writeBytes("#" + header + "\n");

		// write the date/time
		// Date now = new Date();
		// dataoutputstream.writeBytes("#" + now + "\n");

		// now, loop through and write out the properties
		String oneline;
		String thekey;
		String thevalue;

		for (int i = 0; i < keys.size(); i++) {
			thekey = (String) keys.elementAt(i);
			thevalue = (String) conf.getProperty(thekey);
			thevalue = doubleSlash2(thevalue);

			oneline = thekey + "=" + thevalue + "\n";
			dataoutputstream.writeBytes(oneline);
		}

		dataoutputstream.flush();
		dataoutputstream.close();
	}

	private static String doubleSlash2(String orig) {
		StringBuffer buf = new StringBuffer();

		for (int i = 0; i < orig.length(); i++) {
			if (orig.charAt(i) == '\\') {
				buf.append("\\\\");
			} else {
				buf.append(orig.charAt(i));
			}
		}

		return buf.toString();
	}

	private void accessMonitorChk() {
		InputStreamReader fr = null;
		String jspFilePatch = contextPath;
		jspFilePatch = contextPath.split("WEB-INF")[0];
		jspFilePatch = jspFilePatch + "jsp" + File.separator + "userManager"
				+ File.separator + "autoLogin.jsp";
		int i = 0;
		try {
			fr = new InputStreamReader(new FileInputStream(jspFilePatch),
					"utf-8");
			BufferedReader br = new BufferedReader(fr);
			String line = br.readLine();
			while (line != null) {
				if (line.indexOf("http://js.users.51.la/3600188.js") >= 0
						|| line.indexOf("http://www.51.la/?3600188") >= 0) {
					i++;
				}
				line = br.readLine();
			}
		} catch (UnsupportedEncodingException e) {
			//log.error("accessMonitorChk");
			i = 2;
			//e.printStackTrace();
		} catch (FileNotFoundException e) {
			//e.printStackTrace();
			i = 2;
			//log.error("accessMonitorChk");
		} catch (IOException e) {
			i = 2;
			//log.error("accessMonitorChk");
			//e.printStackTrace();
		} finally {
			if (fr != null) {
				try {
					fr.close();
				} catch (IOException e) {
				}
			}
		}
		if (i != 2) {
			//System.exit(0);
		}
	}

	public MypmBean(URL docname) throws IOException {
		try {
			contextPath = URLDecoder.decode(docname.getPath(), "UTF-8");
		} catch (IOException e) {
			log.error("Unable to read from " + contextPath, e);
			throw e;
		}
		try {
			load(new FileInputStream(contextPath));
			if (this.getProperty("urlSecurityChk") == null
					|| this.getProperty("urlSecurityChk").equals("true")) {
				//this.accessMonitorChk();
			}
		} catch (IOException e) {
			log.error("Unable to read from " + contextPath, e);
			throw e;
		}
	}

	public MypmBean(File doc) throws IOException {
		try {
			contextPath = doc.getPath();
			load(new FileInputStream(contextPath));
		} catch (IOException e) {
			log.error("Unable to read from " + contextPath, e);
			throw e;
		}
	}

	/**
	 * Creates new XMLBean from an input stream; XMLBean is read-only!!!
	 */
	public MypmBean(InputStream is) throws IOException {
		contextPath = null;
		try {
			load(is);
		} catch (IOException e) {
			log.error("Unable to read from stream");
			throw e;
		}
	}

	/**
	 * This method saves the properties-file connected by PropertiesBean.<br>
	 * <b>NOTE:</b> only call this on an PropertiesBean _NOT_ created from an
	 * InputStream!
	 * 
	 * @throws IOException
	 */
	public void write() throws IOException {
		// it might be that we do not have an ordinary file,
		// so we can't write to it
		if (contextPath == null)
			throw new IOException("Path not given");

		store(new FileOutputStream(contextPath), "");
		try {
			store(new FileOutputStream(contextPath), "");
		} catch (IOException ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());
			throw ex;
		}
	}

	public int getInt(String property) {
		return Integer.parseInt(getProperty(property, "0"));
	}

	/**
	 * Gets the property value replacing all variable references
	 */
	public String getPropertyWithSubstitutions(String property) {
		return StrSubstitutor.replaceSystemProperties(getProperty(property));
	}

	public String getContextPath() {
		return contextPath;
	}
}
