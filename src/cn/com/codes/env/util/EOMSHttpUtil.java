package cn.com.codes.env.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.ssl.SSLContexts;

import com.opensymphony.webwork.ServletActionContext;

import cn.com.codes.env.util.JsonParserUtil.JSON_TYPE;
import cn.com.codes.object.Cert;


public class EOMSHttpUtil {
	
	private static Log logger = LogFactory.getLog(EOMSHttpUtil.class);
			
	private EOMSHttpUtil eomsHttpUtil;
	
	public SSLConnectionSocketFactory sslConnectionSocketFactory = null;
	
	public EOMSHttpUtil(Cert cert) {
		if(sslConnectionSocketFactory == null){
			FileInputStream fis = null;
			try {
				KeyStore keyStore = KeyStore.getInstance("PKCS12");
				String webRootPath = EOMSHttpUtil.getWebRootPath();
				String filePath = webRootPath + "itest" + File.separator + "env" + File.separator + "cert" + File.separator + cert.getApiCertName();//URLDecoder.decode(resourcePath.getPath(), "UTF-8");
				fis = new FileInputStream(new File(filePath));
				keyStore.load(fis,cert.getCertPwd().toCharArray());
				SSLContext ctx = SSLContexts.custom()
					.loadTrustMaterial(new TrustStrategy(){
						 public boolean isTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			                 return true;
			             }
					})
					.loadKeyMaterial(keyStore, cert.getCertPwd().toCharArray())//PropertyUtil.getProperties("SSL_KEY_PASSWORD").toCharArray()
		            .build();
				sslConnectionSocketFactory = new SSLConnectionSocketFactory(
						ctx,new String[]{PropertyUtil.getProperties("SSL_PROTOCOL")},null,SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				
			}
		}
		
		
	}
	
	public EOMSHttpUtil() {
		
	}
	

	
	public HttpEntity httpPostAtStream(String url,InputStreamEntity entity) {
		CloseableHttpClient httpClient = null;
		if(sslConnectionSocketFactory == null) {
			httpClient = HttpClients.custom().build();
		}else {
			httpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build();
		}
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("Content-Type", "application/tar; charset=UTF-8");
		if (null != entity) {
			httpPost.setEntity(entity);
		}
		JSONObject returnData = new JSONObject();
		HttpEntity responseEntity = null;
		try {
			CloseableHttpResponse response = httpClient.execute(httpPost);
			responseEntity =  response.getEntity();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return responseEntity;
	}
	
	public Object httpPost(String url,  JSONObject containers/**BasicNameValuePair value*/) {
		CloseableHttpClient httpClient = null;
		if(sslConnectionSocketFactory == null) {
			httpClient = HttpClients.custom().build();
		}else {
			httpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build();
		}
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("Content-Type", "application/json; charset=UTF-8");
		if(containers != null){
			StringEntity entity = new StringEntity(containers.toString(),"UTF-8");//解决中文乱码问题    

			
			httpPost.setEntity(entity);
		}
		

		String line = null;
		try {

			CloseableHttpResponse response = httpClient.execute(httpPost);
			HttpEntity responseEntity =  response.getEntity();
			if(responseEntity == null){
				JSONObject returnData = new JSONObject();
				returnData.put("message", "1");
				return returnData.toString();
			}
			InputStream is = responseEntity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {

				return line;
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public static HttpResponse doHttpPost(String url, String dtoJsonStr) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		//JSONObject jsonObject = JSONObject.fromObject(dto);
		//StringBuilder stringBuilder = new StringBuilder();
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("Content-Type", "application/json; charset=utf-8");
		httpPost.setHeader("Accept", "application/json");
		HttpResponse response = null;
		try {
			StringEntity stringEntity = new StringEntity(dtoJsonStr, Charset.forName("utf-8"));
			httpPost.setEntity(stringEntity);
			//HttpClient httpClient = new DefaultHttpClient();
			response = httpClient.execute(httpPost);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getLocalizedMessage();
		}
		return response;
	}
	
	public Object httpGet(String url, HttpParams params) {
		CloseableHttpClient httpClient = null;
		if(sslConnectionSocketFactory == null) {
			httpClient = HttpClients.custom().build();
		}else {
			httpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build();
		}
		HttpGet httpGet = new HttpGet(url);

		String line = null;
		StringBuffer str = new StringBuffer();
		try {

			CloseableHttpResponse response = httpClient.execute(httpGet);
			HttpEntity responseEntity =  response.getEntity();
			InputStream is = responseEntity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {

				str.append(line).append("\n");
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return str;
	}
	
	public Object httpPut(String url) {
		CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build();
		HttpPut httpPut = new HttpPut(url);
		String line = null;
		try {
			CloseableHttpResponse response = httpClient.execute(httpPut);
			HttpEntity responseEntity =  response.getEntity();
			InputStream is = responseEntity.getContent();
			BufferedReader br =  new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				//System.out.println("updateContainer: " + line);
				JSONObject jsonObject = JSONObject.fromObject(line);
				System.out.println("jsonObject: " + jsonObject.toString());
				return line;
			}
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Object httpDelete(String url) {
		CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslConnectionSocketFactory).build();
		HttpDelete httpDelete = new HttpDelete(url);
		String line = null;
		try {
			CloseableHttpResponse response = httpClient.execute(httpDelete);
			HttpEntity responseEntity =  response.getEntity();
			if(responseEntity == null){
				JSONObject returnData = new JSONObject();
				returnData.put("message", "1");
				return returnData.toString();
			}else {
				InputStream is = responseEntity.getContent();
				BufferedReader br =  new BufferedReader(new InputStreamReader(is));
				while ((line = br.readLine()) != null) {
					//System.out.println("updateContainer: " + line);
					
					
					return line;
				}
			}
			
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static String getWebRootPath() {
		ClassLoader classLoader = EOMSHttpUtil.class.getClassLoader();
		if (classLoader == null) {  
	        classLoader = ClassLoader.getSystemClassLoader();
	    }  
	    java.net.URL url = classLoader.getResource("");
	    String rootClassPath = url.getPath() + File.separator;
	    File rootFile = new File(rootClassPath);
	    String webInfoDirPath = rootFile.getParent() + File.separator;  
	    File webInfoDir = new File(webInfoDirPath);
	    String webRootPaht = webInfoDir.getParent() + File.separator;
	    return webRootPaht;
	}
	
	public HttpParams getParameterMapValue(HttpServletRequest request) {
		Map properties = request.getParameterMap();
		Iterator it = properties.entrySet().iterator();
		List<NameValuePair> formparams = new ArrayList<NameValuePair>();
		HttpParams params = new BasicHttpParams();
		while (it.hasNext()) {
			Map.Entry<String, Object> map = (Map.Entry<String, Object>)it.next();
			String name = map.getKey();
			Object value = map.getValue();
			
			
			
		}
		params.setParameter("Hostname","dockerdns1");
		params.setParameter("Image","base");

		params.setParameter("AttachStdin",false);
		params.setParameter("AttachStdout",true);
		params.setParameter("AttachStderr",true);
		params.setParameter("OpenStdin",false);
		params.setParameter("StdinOnce",false);
		params.setParameter("Tty",false);
		params.setParameter("NetworkDisabled", false);
		params.setParameter("StopSignal", "SIGTERM");
		params.setParameter("StopTimeout", 10);
		HashMap map = new HashMap();

		params.setParameter("WorkingDir","");

		return params;
		
		
	}

}
