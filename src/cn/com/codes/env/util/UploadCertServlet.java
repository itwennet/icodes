package cn.com.codes.env.util;

import java.io.File;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.log4j.Logger;

import com.opensymphony.webwork.ServletActionContext;

import cn.com.codes.framework.common.JsonUtil;

@WebServlet("/uploadCert")
public class UploadCertServlet extends HttpServlet {
	
	private static Logger logger = Logger.getLogger(BuildImageServlet.class);
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) {
		
		String path = this.getServletContext().getRealPath("/");
		logger.info("path:" + path);
		response.setContentType("text/plain;charset=utf-8");
        try {
//        	URL resourcePath = UploadCertServlet.class.getClassLoader().getResource("/resource/cert");
//    		String filePath = URLDecoder.decode(resourcePath.getPath(), "UTF-8");
			PrintWriter pw = response.getWriter();
			DiskFileItemFactory diskFactory = new DiskFileItemFactory();
            // threshold 极限、临界值，即硬盘缓存 1M
            //diskFactory.setSizeThreshold(4 * 1024);
            // repository 贮藏室，即临时文件目录
            //diskFactory.setRepository(new File(filePath));
            ServletFileUpload upload = new ServletFileUpload(diskFactory);
            // 设置允许上传的最大文件大小 4M
            upload.setSizeMax(4 * 1024 * 1024);
            // 解析HTTP请求消息头
            List<FileItem> fileItems = upload.parseRequest(request);
            Iterator<FileItem> iter = fileItems.iterator();
            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                if (!item.isFormField()) {
                	uploadCert(item, pw);
                }
            }
            pw.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void uploadCert(FileItem item, PrintWriter pw) {
		Map<String, Object> map = new HashMap<String, Object>();
		// 此时的文件名包含了完整的路径，得注意加工一下
        String filename = item.getName();
        logger.info("filename：" + filename);
        int index = filename.lastIndexOf("\\");
        filename = filename.substring(index + 1, filename.length());
        logger.info("filenamesubstring：" + filename);
        long fileSize = item.getSize();
        if ("".equals(filename) && fileSize == 0) {
            return;
        }
        try {
        	String fileType = filename.substring(filename.lastIndexOf("."), filename.length());
			Date date = new Date();
			long dateTime = date.getTime();
			String filePath = this.getServletContext().getRealPath("/") + "itest"+File.separator+"env"+File.separator+"cert";
            File uploadFile = new File(filePath + File.separator + dateTime + fileType);
            File uploadFile2 = new File(filePath);
            if(!uploadFile2.exists()){
            	uploadFile2.mkdirs();
            }
			item.write(uploadFile);
			map.put("oprateResult", "success");
			map.put("certName", filename);
			map.put("apiCertName", dateTime + fileType);
			map.put("filePath", filePath + "/" + filename);
			pw.write(JsonUtil.toJson(map).toString());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			map.put("oprateResult", "failed");
			pw.write(JsonUtil.toJson(map).toString());
		}
	}
}
