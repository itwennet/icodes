package cn.com.codes.env.service.impl;



import java.util.List;

import cn.com.codes.env.dto.EnvDto;
import cn.com.codes.env.service.EnvService;
import cn.com.codes.framework.app.services.BaseServiceImpl;
import cn.com.codes.framework.hibernate.HibernateGenericController;
import cn.com.codes.object.Cert;
import cn.com.codes.object.Image;

public class EnvServiceImpl extends BaseServiceImpl implements EnvService {
	
	@Override
	public List<Image> getImageList(EnvDto envDto) {
		String hql = "from Image where 1=1 order by createDate desc";
		List<Image> imageList = this.findByHql(hql, null);
		return imageList;
	}

	@Override
	public Boolean saveImage(EnvDto envDto) {

		try {
			this.saveOrUpdate(envDto.getImage());
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void saveAppInfo(EnvDto envDto) {
		// TODO Auto-generated method stub
		String hql="update Image set appId=?, appName=?, appStatus=?, appAccessIp=?, appPort=?, containersPort=?, appEnvVar=?, appLinkContainer=? where id=?";
		super.getHibernateGenericController().executeUpdate(hql, envDto.getImage().getAppId(), envDto.getImage().getAppName(),envDto.getImage().getAppStatus(), envDto.getImage().getAppAccessIp(),
				envDto.getImage().getAppPort(), envDto.getImage().getContainersPort(), envDto.getImage().getAppEnvVar(), envDto.getImage().getAppLinkContainer(), envDto.getImage().getId());
	}

	@Override
	public void updateAppStatus(EnvDto envDto) {
		// TODO Auto-generated method stub
		String hql="update Image set appStatus=? where id=?";
		super.getHibernateGenericController().executeUpdate(hql, envDto.getImage().getAppStatus(),envDto.getImage().getId());
	}

	@Override
	public void saveBuildImageInfo(EnvDto envDto) {

		this.saveOrUpdate(envDto.getImage());
	}

	@Override
	public List<Cert> getCertList(EnvDto envDto) {
		// TODO Auto-generated method stub
		String hql = "from Cert where 1=1 order by uploadDate desc";
		List<Cert> certList = this.findByHql(hql, null);
		return certList;
	}
	
	

	@Override
	public Boolean saveCert(EnvDto envDto) {
		// TODO Auto-generated method stub
		try {
			this.saveOrUpdate(envDto.getCert());
			return true;
		} catch(Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<Cert> getCertList() {
		// TODO Auto-generated method stub
		String hql = "from Cert where 1=1 order by uploadDate desc";
		List<Cert> certList = this.findByHql(hql, null);
		return certList;
	}

	@Override
	public void deleteImage(EnvDto envDto) {
		// TODO Auto-generated method stub
		String hql = "delete from Image where id=?";
		this.executeUpdateByHql(hql, new Object[]{envDto.getImage().getId()});
	}

	

}
