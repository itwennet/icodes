package cn.com.codes.env.web;

import cn.com.codes.env.blh.EnvBlh;
import cn.com.codes.env.dto.EnvDto;
import cn.com.codes.framework.app.blh.BaseBizLogicHandler;
import cn.com.codes.framework.exception.BaseException;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.framework.web.action.BaseAction;

public class EnvAction extends BaseAction<EnvBlh> {
	
	private static final long serialVersionUID = 1L;
	
	private EnvDto dto = new EnvDto();
	
	private EnvBlh envBlh;

	@Override
	protected void _prepareRequest(BusiRequestEvent reqEvent) throws BaseException {
		// TODO Auto-generated method stub
		reqEvent.setDto(dto);
	}

	public EnvDto getDto() {
		return dto;
	}

	public void setDto(EnvDto dto) {
		this.dto = dto;
	}

	public EnvBlh getEnvBlh() {
		return envBlh;
	}

	public void setEnvBlh(EnvBlh envBlh) {
		this.envBlh = envBlh;
	}
	
	public  BaseBizLogicHandler getBlh(){
		return envBlh;
	}

}
