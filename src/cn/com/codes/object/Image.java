package cn.com.codes.object;

import cn.com.codes.framework.transmission.JsonInterface;

public class Image implements  JsonInterface {
	
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	private String imageId;
	
	private String imageName;
	
	private String imageDesc;
	
	private String tag;
	
	private Integer installNum;
	
	private String uploader;
	
	private String filePath;
	
	private String imageFileName;
	
	private String fileType;
	
	private String createDate;
	
	private String remarks;
	
	private String serverIp;
	
	private String appId;
	
	private String appName;
	
	private String appAccessIp;
	
	private String appStatus;
	
	private String appPort;
	
	private String containersPort;
	
	private String appEnvVar;
	
	private String appLinkContainer;
	
	public Image() {
		
	}
	
	public Image(String id, String imageId, String imageName, String imageDesc, String tag, Integer installNum, String uploader, String createDate, String remarks, String serverIp) {
		this.id = id;
		this.imageId = imageId;
		this.imageName = imageName;
		this.imageDesc = imageDesc;
		this.tag = tag;
		this.installNum = installNum;
		this.uploader = uploader;
		this.createDate = createDate;
		this.remarks = remarks;
		this.serverIp = serverIp;
	}
	
	

	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getImageId() {
		return imageId;
	}



	public void setImageId(String imageId) {
		this.imageId = imageId;
	}



	public String getImageName() {
		return imageName;
	}



	public void setImageName(String imageName) {
		this.imageName = imageName;
	}



	public String getImageDesc() {
		return imageDesc;
	}



	public void setImageDesc(String imageDesc) {
		this.imageDesc = imageDesc;
	}



	public String getTag() {
		return tag;
	}



	public void setTag(String tag) {
		this.tag = tag;
	}



	public Integer getInstallNum() {
		return installNum;
	}



	public void setInstallNum(Integer installNum) {
		this.installNum = installNum;
	}



	public String getUploader() {
		return uploader;
	}



	public void setUploader(String uploader) {
		this.uploader = uploader;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	
	public String getImageFileName() {
		return imageFileName;
	}

	public void setImageFileName(String imageFileName) {
		this.imageFileName = imageFileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getCreateDate() {
		return createDate;
	}



	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getServerIp() {
		return serverIp;
	}



	public void setServerIp(String serverIp) {
		this.serverIp = serverIp;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppStatus() {
		return appStatus;
	}

	public void setAppStatus(String appStatus) {
		this.appStatus = appStatus;
	}

	public String getAppAccessIp() {
		return appAccessIp;
	}

	public void setAppAccessIp(String appAccessIp) {
		this.appAccessIp = appAccessIp;
	}

	public String getAppPort() {
		return appPort;
	}

	public void setAppPort(String appPort) {
		this.appPort = appPort;
	}

	public String getContainersPort() {
		return containersPort;
	}

	public void setContainersPort(String containersPort) {
		this.containersPort = containersPort;
	}

	public String getAppEnvVar() {
		return appEnvVar;
	}

	public void setAppEnvVar(String appEnvVar) {
		this.appEnvVar = appEnvVar;
	}

	public String getAppLinkContainer() {
		return appLinkContainer;
	}

	public void setAppLinkContainer(String appLinkContainer) {
		this.appLinkContainer = appLinkContainer;
	}

	@Override
	public String toStrUpdateInit() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toStrList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String toStrUpdateRest() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void toString(StringBuffer bf) {
		// TODO Auto-generated method stub
		
	}

}
