package cn.com.codes.caseManager.dto;

import java.util.ArrayList;
import java.util.List;

import cn.com.codes.object.TestCaseInfo;

public class ImpCaseFromExpVo {
	
	
	
	private List<TestCaseInfo> newCaseList;
	
	private List<TestCaseInfo> offLineCaseList;
	
	private List<TestCaseInfo> offLineExeCaseList;

	public List<TestCaseInfo> getNewCaseList() {
		if( newCaseList==null) {
			newCaseList=new ArrayList<TestCaseInfo>();
		}
		return newCaseList;
	}

	public void setNewCaseList(List<TestCaseInfo> newCaseList) {
		this.newCaseList = newCaseList;
	}

	public List<TestCaseInfo> getOffLineCaseList() {
		if( offLineCaseList==null) {
			offLineCaseList=new ArrayList<TestCaseInfo>();
		}
		return offLineCaseList;
	}

	public void setOffLineCaseList(List<TestCaseInfo> offLineCaseList) {
		this.offLineCaseList = offLineCaseList;
	}

	public List<TestCaseInfo> getOffLineExeCaseList() {
		if( offLineExeCaseList==null) {
			offLineExeCaseList=new ArrayList<TestCaseInfo>();
		}
		return offLineExeCaseList;
	}

	public void setOffLineExeCaseList(List<TestCaseInfo> offLineExeCaseList) {
		this.offLineExeCaseList = offLineExeCaseList;
	}



}
