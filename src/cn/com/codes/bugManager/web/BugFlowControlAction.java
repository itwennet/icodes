package cn.com.codes.bugManager.web;

import java.util.HashMap;

import cn.com.codes.bugManager.blh.BugFlowControlBlh;
import cn.com.codes.bugManager.dto.BugManagerDto;
import cn.com.codes.framework.app.blh.BaseBizLogicHandler;
import cn.com.codes.framework.exception.BaseException;
import cn.com.codes.framework.transmission.events.BusiRequestEvent;
import cn.com.codes.framework.web.action.BaseAction;

public class BugFlowControlAction extends BaseAction<BugFlowControlBlh> {
	
	BugManagerDto dto;
	
	private BugFlowControlBlh bugFlowControlBlh;

	@Override
	protected void _prepareRequest(BusiRequestEvent reqEvent) throws BaseException {
		// TODO Auto-generated method stub
		if(dto==null){
			dto = new BugManagerDto();
		}
		reqEvent.setDto(dto);
	}
	
	protected String _processResponse() throws BaseException {
		HashMap<?, ?> displayData = (HashMap<?, ?>) _getDisplayData();
		return forwardPage(displayData);
	}

	public BugManagerDto getDto() {
		return dto;
	}

	public void setDto(BugManagerDto dto) {
		this.dto = dto;
	}

	public BugFlowControlBlh getBugFlowControlBlh() {
		return bugFlowControlBlh;
	}

	public void setBugFlowControlBlh(BugFlowControlBlh bugFlowControlBlh) {
		this.bugFlowControlBlh = bugFlowControlBlh;
	}

	public  BaseBizLogicHandler getBlh(){
		return bugFlowControlBlh;
	}
	

}
