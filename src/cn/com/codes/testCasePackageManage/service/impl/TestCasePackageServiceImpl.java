package cn.com.codes.testCasePackageManage.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;

import cn.com.codes.common.SecurityContextHolderHelp;
import cn.com.codes.common.util.StringUtils;
import cn.com.codes.framework.app.services.BaseServiceImpl;
import cn.com.codes.framework.hibernate.HibernateGenericController;
import cn.com.codes.framework.security.VisitUser;
import cn.com.codes.framework.security.filter.SecurityContextHolder;
import cn.com.codes.object.TestCasePackage;
import cn.com.codes.object.TestCase_CasePkg;
import cn.com.codes.object.TestcasepkgOperationHistory;
import cn.com.codes.object.UserTestCasePkg;
import cn.com.codes.overview.dto.DataVo;
import cn.com.codes.testCasePackageManage.dto.TestCasePackageDto;
import cn.com.codes.testCasePackageManage.service.TestCasePackageService;

public class TestCasePackageServiceImpl extends BaseServiceImpl implements TestCasePackageService {
	
	private static Logger loger = Logger.getLogger(TestCasePackageServiceImpl.class);
	private TestCasePackageService testCasePackageService;


	public void saveTestCasePackage(TestCasePackageDto testCasePackageDto){
		//保存t_testcasepackage表
		this.add(testCasePackageDto.getTestCasePackage());
		
		//保存t_user_testcasepkg表
		this.saveUserTestCasePkg(testCasePackageDto);
		
	}
	
	public void updateTestCasePackage(TestCasePackageDto testCasePackageDto){
		//更新t_testcasepackage表
		this.update(testCasePackageDto.getTestCasePackage());
		
		if(!StringUtils.isNullOrEmpty(testCasePackageDto.getTestCasePackage().getPackageId())){
			//更新t_user_testcasepkg表
			//先删除t_user_testcasepkg表原有数据
			this.deleteUserTestCasePkg(testCasePackageDto.getTestCasePackage().getPackageId());
			
		}
	
		this.saveUserTestCasePkg(testCasePackageDto);
	}
	
	public void deleteTestCasePkgById(String packageId){
	   this.delete(TestCasePackage.class, packageId);
	   //删除t_user_testcasepkg表
	   this.deleteUserTestCasePkg(packageId);
	   //删除t_testcase_casepkg表
	   this.deleteTestCase_CasePkg(packageId);
	}

	public String[] getUserIdsByPackageId(TestCasePackageDto testCasePackageDto){
		String hql = "from UserTestCasePkg  where packageId=?";
		@SuppressWarnings("unchecked")
		List<UserTestCasePkg> userTestCasePkgs = this.findByHql(hql, testCasePackageDto.getTestCasePackage().getPackageId());
		if(userTestCasePkgs != null && userTestCasePkgs.size() > 0){
			String[] userIds = new String[userTestCasePkgs.size()];
			for(int i=0;i<userTestCasePkgs.size();i++){
				userIds[i] = userTestCasePkgs.get(i).getUserId();
			}
			return userIds;
		}else{
			return null;
		}
	}
	
	public String[] getTestCaseIdsByPackageId(TestCasePackageDto testCasePackageDto){
		String hql = "from TestCase_CasePkg  where packageId=?";
		@SuppressWarnings("unchecked")
		List<TestCase_CasePkg> testCase_CasePkg = this.findByHql(hql, testCasePackageDto.getTestCasePackage().getPackageId());
		if(testCase_CasePkg != null && testCase_CasePkg.size() > 0){
			String[] testCaseIds = new String[testCase_CasePkg.size()];
			for(int i=0;i<testCase_CasePkg.size();i++){
				testCaseIds[i] = testCase_CasePkg.get(i).getTestCaseId();
			}
			return testCaseIds;
		}else{
			return null;
		}
	}
	
	private void saveUserTestCasePkg(TestCasePackageDto testCasePackageDto) {
		List<UserTestCasePkg> userTestCasePkgs = new ArrayList<UserTestCasePkg>();

		if(null !=testCasePackageDto.getSelectedUserIds()){
			String[] selectedIdList = testCasePackageDto.getSelectedUserIds().split(",");
			for(String selId:selectedIdList){
				UserTestCasePkg userTestCasePkg = new UserTestCasePkg();
				userTestCasePkg.setPackageId(testCasePackageDto.getTestCasePackage().getPackageId());
				userTestCasePkg.setUserId(selId);
				userTestCasePkgs.add(userTestCasePkg);
			}
		this.batchSaveOrUpdate(userTestCasePkgs);
	  }
	}

	  //删除t_user_testcasepkg表
	private void deleteUserTestCasePkg(String packageId) {
		this.executeUpdateByHql("delete from UserTestCasePkg where packageId =? ", new Object[]{packageId});
	}
	
	
	public void saveTestCase_CasePkg(TestCasePackageDto testCasePackageDto) {
		String userId = SecurityContextHolder.getContext().getVisit().getUserInfo(VisitUser.class).getId();
		String userName = SecurityContextHolder.getContext().getVisit().getUserInfo(VisitUser.class).getName();
		String taskId = SecurityContextHolderHelp.getCurrTaksId(); 
		List<TestCase_CasePkg> testCase_CasePkgs = new ArrayList<TestCase_CasePkg>();
		List<TestcasepkgOperationHistory> testcasepkgHistoryLists = new ArrayList<TestcasepkgOperationHistory>();
        //选中的testcaseId		
		String[] selectedCaseIdList = testCasePackageDto.getSelectedTestCaseIds().split(",");
		Integer notExeCount = 0;
		Integer exeCount = 0;
		TestCasePackage testCasePackage = new TestCasePackage();
		
		//获取该用例包的已执行用例数
		List<TestCasePackage> testcasepkgList=this.findByProperties(TestCasePackage.class, new String[]{ "id"},new Object[]{testCasePackageDto.getTestCasePackage().getPackageId()});
		
		if(testcasepkgList.size()>0){
			testCasePackage = testcasepkgList.get(0);
		}
		
		//查询原有的数据
		List<TestCase_CasePkg> initCaseIdList = this.findByProperties(TestCase_CasePkg.class,new String[]{ "packageId"},new Object[]{testCasePackageDto.getTestCasePackage().getPackageId()});
		//新增的用例
		//List<TestCase_CasePkg> addTCaseIdList = new ArrayList<TestCase_CasePkg>();
		//取消用例
		//List<TestCase_CasePkg> cancelTCaseIdList = tCaseIdList; //循环时 当匹配到选中的是已存在用例 ，则从cancellist中删除；循环完成之后，剩余的用例数据则是取消用例
		
		 Boolean existFlag = false; //true:找到已存在用例   false:新增用例
		for(int i=0;i<selectedCaseIdList.length;i++){
			existFlag = false;
			for(int j=0;j<initCaseIdList.size();j++){
				if(selectedCaseIdList[i].equals(initCaseIdList.get(j).getTestCaseId())){
					//保存原来用例的执行状态用于计算已执行用例数
					if(initCaseIdList.get(j).getExecStatus() !=null && initCaseIdList.get(j).getExecStatus() != 1){
						exeCount++;
					}
					existFlag = true;
					
					//移除
					initCaseIdList.remove(initCaseIdList.get(j)); 
					break;
				}
			}
			
			//新增用例包关联用例
			if(!existFlag){
				//新增用例--用例包关联表数据
				TestCase_CasePkg testCase_CasePkgNew = new TestCase_CasePkg();
				testCase_CasePkgNew.setPackageId(testCasePackageDto.getTestCasePackage().getPackageId());
				testCase_CasePkgNew.setTestCaseId(selectedCaseIdList[i]);
				testCase_CasePkgs.add(testCase_CasePkgNew);
				
				//添加历史记录--用例包新增用例
				TestcasepkgOperationHistory testcasepkgHistory = new TestcasepkgOperationHistory();
				testcasepkgHistory.setPackageId(testCasePackageDto.getTestCasePackage().getPackageId());
				testcasepkgHistory.setTaskId(taskId);
				testcasepkgHistory.setTestCaseId(Long.parseLong(selectedCaseIdList[i]));
				testcasepkgHistory.setOperationPersonId(userId);
				testcasepkgHistory.setOperationPersonName(userName);
				testcasepkgHistory.setOperationTime(new Date());
				testcasepkgHistory.setOperationType("1");  //删除：0   增加：1
				testcasepkgHistoryLists.add(testcasepkgHistory);
			}
		}
		
		//删除关联表中未选中的用例记录
		if(initCaseIdList.size()>0){
			for(int n=0;n<initCaseIdList.size(); n++){
				//添加历史记录--用例包删除用例
				TestcasepkgOperationHistory testcasepkgHistory = new TestcasepkgOperationHistory();
				testcasepkgHistory.setPackageId(testCasePackageDto.getTestCasePackage().getPackageId());
				testcasepkgHistory.setTaskId(taskId);
				testcasepkgHistory.setTestCaseId(Long.parseLong(initCaseIdList.get(n).getTestCaseId()));
				testcasepkgHistory.setOperationPersonId(userId);
				testcasepkgHistory.setOperationPersonName(userName);
				testcasepkgHistory.setOperationTime(new Date());
				testcasepkgHistory.setOperationType("0");  //删除：0   增加：1
				testcasepkgHistoryLists.add(testcasepkgHistory);
				this.executeUpdateByHql("delete from TestCase_CasePkg where packageId =? and testCaseId=?", new Object[]{testCasePackageDto.getTestCasePackage().getPackageId(),initCaseIdList.get(n).getTestCaseId()});
			}
		}
		//批量更新用例-用例包关联表
		this.batchSaveOrUpdate(testCase_CasePkgs);
		
		//保存操作历史
		this.batchSaveOrUpdate(testcasepkgHistoryLists);
		
		notExeCount = selectedCaseIdList.length-exeCount;
		//更新t_testcasepackage表的用例已执行数和用例总数
		testCasePackage.setExeCount(exeCount);
		testCasePackage.setNotExeCount(notExeCount);
		this.saveOrUpdate(testCasePackage);
//	  }
	}
	
	 //删除t_testcase_casepkg表
	private void deleteTestCase_CasePkg(String packageId){
		this.executeUpdateByHql("delete from TestCase_CasePkg where packageId =? ", new Object[]{packageId});
	}
	
	 //删除t_testcase_casepkg表  通过pkgCaseId查询要删除的数据
	public void delTestCase_CasePkgByPkgCaseId(String pkgCaseId,String selCaseId,String packageId){
		  
				String userId = SecurityContextHolder.getContext().getVisit().getUserInfo(VisitUser.class).getId();
				String userName = SecurityContextHolder.getContext().getVisit().getUserInfo(VisitUser.class).getName();
				String taskId = SecurityContextHolderHelp.getCurrTaksId(); 
				
				List<TestcasepkgOperationHistory> testcasepkgHistoryLists = new ArrayList<TestcasepkgOperationHistory>();
				
				TestcasepkgOperationHistory testcasepkgHistory = new TestcasepkgOperationHistory();
				testcasepkgHistory.setPackageId(packageId);
				testcasepkgHistory.setTaskId(taskId);
				testcasepkgHistory.setTestCaseId(Long.parseLong(selCaseId));
				testcasepkgHistory.setOperationPersonId(userId);
				testcasepkgHistory.setOperationPersonName(userName);
				testcasepkgHistory.setOperationTime(new Date());
				testcasepkgHistory.setOperationType("0");  //删除：0   增加：1
				testcasepkgHistoryLists.add(testcasepkgHistory);
				
			this.executeUpdateByHql("delete from TestCase_CasePkg where pkgCaseId =? ", new Object[]{pkgCaseId});
			//保存操作历史
			this.batchSaveOrUpdate(testcasepkgHistoryLists);
			
			//移除用例时，更新未执行用例数
			TestCasePackage testCasePackage = new TestCasePackage();
			List<TestCasePackage> testcasepkgList=this.findByProperties(TestCasePackage.class, new String[]{ "id"},new Object[]{packageId});
			if(testcasepkgList.size()>0){
				testCasePackage = testcasepkgList.get(0);
				Integer notExeCount = testCasePackage.getNotExeCount();
				if(notExeCount !=null){
					if(notExeCount !=0){
						notExeCount--;
						testCasePackage.setNotExeCount(notExeCount);
						this.update(testCasePackage);
					}
					
				}
			}
	}
	
	//查询某个用例包已分配的所有用例
	public  List<TestCasePackageDto> getSelTestCasesByPkgId(TestCasePackageDto testCasePackageDto){
		StringBuilder hqlStr = new StringBuilder();
		hqlStr.append("SELECT new cn.com.codes.testCasePackageManage.dto.TestCasePackageDto(tc.pkgCaseId,tc.packageId,  tt.taskId,   tt.moduleId, " + 
			 " tt.testCaseId,   tt.createrId,   tt.prefixCondition, tt.testCaseDes,   tt.isReleased,   tt.creatdate, " +
			 " tt.attachUrl,   tt.caseTypeId,   tt.priId,   tt.weight, tc.execStatus as testStatus, " +
			 " tt.moduleNum,  tc.executorId as auditId, tt.OperDataRichText, tt.expResult) FROM cn.com.codes.object.TestCase_CasePkg tc " + 
             ",cn.com.codes.object.TestCaseInfo tt where tc.testCaseId = tt.testCaseId ");
                      
	      if(null != testCasePackageDto.getTestCasePackage()){
			if(null != testCasePackageDto.getTestCasePackage().getPackageId()){
				hqlStr.append(" and tc.packageId = '" + testCasePackageDto.getTestCasePackage().getPackageId() + "'");
			}
		  }
	      
	      if(null != testCasePackageDto.getQueryParam()){
	    	  if(!testCasePackageDto.getQueryParam().equals("-1")){
	    		  if(testCasePackageDto.getQueryParam().equals("1")){
	    			  hqlStr.append(" and ( tc.execStatus = '" + testCasePackageDto.getQueryParam() + "'");
	    			  hqlStr.append(" or tc.execStatus is null )");
		    	  }else{
						hqlStr.append(" and tc.execStatus = '" + testCasePackageDto.getQueryParam() + "'");
		    	  }
	    	  }
		  }
	      
	     List<TestCasePackageDto> lists = this.findByHql(hqlStr.toString(), null);
	     return lists;
	}
	
	//查询某个用例包已分配用例--分页
	public  List<TestCasePackageDto> getPaginationForSelTestCasesByPkgId(TestCasePackageDto testCasePackageDto){
		StringBuilder hqlStr = new StringBuilder();
		hqlStr.append("SELECT new cn.com.codes.testCasePackageManage.dto.TestCasePackageDto(tc.pkgCaseId,tc.packageId,  tt.taskId,   tt.moduleId, " + 
			 " tt.testCaseId,   tt.createrId,   tt.prefixCondition, tt.testCaseDes,   tt.isReleased,   tt.creatdate, " +
			 " tt.attachUrl,   tt.caseTypeId,   tt.priId,   tt.weight, tc.execStatus as testStatus, " +
			 " tt.moduleNum,  tc.executorId as auditId, tt.OperDataRichText, tt.expResult) FROM cn.com.codes.object.TestCase_CasePkg tc " + 
             ",cn.com.codes.object.TestCaseInfo tt where tc.testCaseId = tt.testCaseId ");
                      
	      if(null != testCasePackageDto.getTestCasePackage()){
			if(null != testCasePackageDto.getTestCasePackage().getPackageId()){
				hqlStr.append(" and tc.packageId = '" + testCasePackageDto.getTestCasePackage().getPackageId() + "'");
			}
		  }
	      
	      if(null != testCasePackageDto.getQueryParam()){
	    	  if(!testCasePackageDto.getQueryParam().equals("-1")){
	    		  if(testCasePackageDto.getQueryParam().equals("1")){
	    			  hqlStr.append(" and ( tc.execStatus = '" + testCasePackageDto.getQueryParam() + "'");
	    			  hqlStr.append(" or tc.execStatus is null )");
		    	  }else{
						hqlStr.append(" and tc.execStatus = '" + testCasePackageDto.getQueryParam() + "'");
		    	  }
	    	  }
		  }
	     
	      testCasePackageDto.setHql(hqlStr.toString());
		  List<TestCasePackageDto> lists = this.findByHqlWithValuesMap(testCasePackageDto);
	      return lists;
	}
	
	public boolean reNameChkInTask(String objName,final String nameVal,final String namePropName,String idPropName,final String idPropVal,final String taskId){
		
		StringBuffer hql = new StringBuffer();
		hql.append("select count(").append(idPropName==null?"*":idPropName).append(")");
		hql.append(" from ").append(objName);
		hql.append(" where ").append(namePropName).append("=? and taskId=?");
		if(idPropName!=null&&!"".equals(idPropName.trim())&&idPropVal!=null&&!"".equals(idPropVal.trim())){
			hql.append(" and  ").append(idPropName).append("!=?");
		}
		final String countHql= hql.toString();
		List countlist = (List)this.hibernateGenericController.getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session)throws HibernateException {
				Query queryObject = session.createQuery(countHql).setCacheable(true);
				queryObject.setParameter(0, nameVal);
				queryObject.setParameter(1, taskId);
					if(idPropVal!=null&&!"".equals(idPropVal.trim())) {
						queryObject.setParameter(2, idPropVal);
					}
				return queryObject.list();
			}
		}, true);
		int count = HibernateGenericController.toLong(countlist.get(0)).intValue();
		return count>0?true:false;
	}	
	
	public DataVo getBugStaticsByPkgId(TestCasePackageDto testCasePackageDto){
		DataVo dataVo = new DataVo();
		String sql = "SELECT * FROM ( ( SELECT count(*) AS allcount FROM t_testcase_casepkg WHERE packageId = ? ) AS aa, " +
			  "( SELECT count(*) AS waitAuditCount FROM t_testcase_casepkg WHERE execStatus =0 AND packageId =? ) bb, " + 
			  "( SELECT count(*) AS passCount FROM t_testcase_casepkg WHERE ( execStatus = 2 ) AND packageId = ? ) dd, " + 
			  "(SELECT count(*) AS failedCount FROM t_testcase_casepkg WHERE execStatus = 3 AND packageId = ? ) ee, " + 
			  "( SELECT count(*) AS invalidCount FROM t_testcase_casepkg WHERE execStatus = 4 AND packageId = ?  ) ff, " + 
			  "( SELECT count(*) AS blockCount FROM t_testcase_casepkg WHERE execStatus = 5 AND packageId = ? ) gg," + 
			  "( SELECT count(*) AS waitModifyCount FROM t_testcase_casepkg WHERE execStatus = 6 AND packageId = ? ) hh ) " ;
		String pkgId = testCasePackageDto.getTestCasePackage().getPackageId();
		List<Object[]> countlist = this.findBySql(sql,null,pkgId,pkgId,pkgId,pkgId,pkgId,pkgId,pkgId);
		
		dataVo.setAllCount(Integer.parseInt(countlist.get(0)[0].toString()));
		dataVo.setWaitAuditCount(Integer.parseInt(countlist.get(0)[1].toString()));
		dataVo.setPassCount(Integer.parseInt(countlist.get(0)[2].toString()));
		dataVo.setFailedCount(Integer.parseInt(countlist.get(0)[3].toString()));
		dataVo.setInvalidCount(Integer.parseInt(countlist.get(0)[4].toString()));
		dataVo.setBlockCount(Integer.parseInt(countlist.get(0)[5].toString()));
		dataVo.setWaitModifyCount(Integer.parseInt(countlist.get(0)[6].toString()));
		
		return dataVo;
	}
	
	//执行用例成功后  更新已执行用例数 未执行用例数
	public void updateTestCasePkgExeCount(String testCasepkgId){
		List<TestCasePackage> testcasepkgList=this.findByProperties(TestCasePackage.class, new String[]{ "id"},new Object[]{testCasepkgId});
		TestCasePackage testCasePackage = new TestCasePackage();
		Integer exeCount = 0 ;
		 
		
		if(testcasepkgList.size()>0){
			testCasePackage = testcasepkgList.get(0);
			
			if(testCasePackage.getExeCount() != null){
				exeCount = testCasePackage.getExeCount();	
			}
			exeCount++;
			
			Integer notExeCount =  testCasePackage.getNotExeCount();
			if(notExeCount != null){
				if(notExeCount!=0){
					notExeCount--;
				}
			}else{
				notExeCount = 0 ;
			}
			
		
			testCasePackage.setExeCount(exeCount);
			testCasePackage.setNotExeCount(notExeCount);
			this.update(testCasePackage);
	    }
		
		
	}
}