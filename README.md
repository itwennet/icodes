# Codes

CODES 介绍

 开源免费一站式研发管理平台
![输入图片说明](WebRoot/itest/images/5.png)
 
 [官网及介绍](http://icodes.work)

# codes 功能 
Codes  是一个 高效、简洁、轻量的一站式研发管理平台。包含需求管理，任务管理，测试管理，缺陷管理，自动化测试，cicd 等功能；
Codes  帮助企业加速融合研发、测试、运维一体化进程
常态下,刀耕火种的Test环节给自动化的Dev与Ops 踩下了刹车。
Codes 以技术最薄弱，最不被重视的测试为发力点，通过落地敏捷测试打通了研发与运维中间的枢钮润滑环节。
解决了Test 在DevOps快速迭代中的木桶效应，促进了研发、测试、运维一体化融合进程。

 **商业版不限功能，本地安装只限用户数，30个用户免费** ;社区版当前只开放了测试跟踪管理(主要功能用例管理，缺陷管理)，后续接着分离其他功能代码出来

# codes 的优势
开源：基于开源、兼容开源；按月发布新版本；

数据一站式：通过一站式式研发管理平台，精准沉淀研发过程全场景数据，以数据为管理抓手，实现研发效能可度量，可数字化。协助技术团队研发效能升级；

codes协作一站式：加速产品、研发、测试、运维一体化融合，杜绝“割裂”管理，提升整体研发效能；高效、简洁、轻量：0探索，快速落地，开箱即用的devTestOps解决方案；

# 初始化说明
1.src\resource\spring\configure.properties 数据库配置文

2.src\sql\codes.sql 数据库脚本（初始codes 的超级管理员为admin ,密码也是admin）

3.codes 社区版 由老项目代码改写而来，推荐使用tomcat 8.5 +;开发环境eclipse +jdk7

# UI 展示 
![输入图片说明](WebRoot/itest/images/1.png)
![输入图片说明](WebRoot/itest/images/2.png)
![输入图片说明](WebRoot/itest/images/3.jpg)
![输入图片说明](WebRoot/itest/images/4.png)
# 技术栈
后端: Spring 
前端: jsp
中间件: MySQL, activeMq
基础设施: Docker 

#  快速开始
只需要2C2G 即可跑得起来
以root执行如下curl：

curl -s https://download.icodes.work/codes_scripts/codes_base_1.1.0GA_u7_install_upgrade_on_docker-compose.sh -o install_codes.sh && bash install_codes.sh