<%@ page contentType="text/html; charset=UTF-8"%>
<%@ page import="java.lang.Exception"%>
<%@ page import="java.io.OutputStream"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="cn.com.codes.framework.exception.BaseException"%>
<%@ page language="java"%>
<%@ page isErrorPage="true"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%
	response.addHeader("Cache-Control", "no-cache");
	response.addHeader("Expires", "Thu,  01 Jan   1970 00:00:01 GMT");
	Exception ex = null;
	if (request.getAttribute("EXP_INFO") != null) {
		ex = (Exception) request.getAttribute("EXP_INFO");
	}
	String isWriteErr = application.getInitParameter("isWriteErr");
	PrintWriter pw;
	//System.out.println("==========================sys int ");
%>
<HTML>
<script Language="JavaScript">
    function reback(){
    	var reUrl= document.referrer;
    	if(reUrl!="")
    		window.location=reUrl;
    	else
       		history.back();
    }
    var reconverScene = "<%=request.getAttribute("MypmTestPermission")%>";
    if(reconverScene!="null"){
    	parent.goMyHome();
    }
</script>
<HEAD>
	<TITLE>系统异常</TITLE>
	<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
</HEAD>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" bgcolor="#ffffff" style="text-align: center">
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
	<center>
		<div style="margin:0 auto;text-align:center;width:685px;height:260px;background:url(<%=request.getContextPath()%>/jsp/common/images/error_bj.gif);"></div>
		<div align="center" style="margin-top:-170px;">
			<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<td height="50" align="center"><span id="tip"><font style="color:blue">提醒:</font></span></td>
				</tr>
				<tr>
					<td>
						<table width="100%" border="0" align="center" bordercolordark="#FFFFFF" bordercolorlight="#cccccc" cellpadding="25" cellspacing="0" height="100%">
							<tr>
								<td height="30" nowrap align="center" valign="middle">&nbsp;&nbsp;
									<font color="blue"> 
									<%
 										if(ex != null && !(ex instanceof NullPointerException)){
 											if(ex.getMessage() != null	&& ex.getMessage().endsWith("您没有当前操作权限")){
 												if("true".equals(isWriteErr)){
 													out.print(ex.getMessage());
 												}else{
 													out.print("您没有当前操作权限！");
 												}
 											}else{
 												if(ex.getMessage().indexOf("JDBC") > 0){
 													out.print("系统正忙请重试");
 												}else{
 													out.print(ex.getMessage());
 												}
 											}
 										}else{
 											if(request.getParameter("_error_message")!=null&&!"null".equals(request.getParameter("_error_message"))){
 												if("no permission to access cuurent directory".equals(request.getParameter("_error_message"))){
 													out.print("您没有当前目录访问权限");
 												}else{
 													out.print(request.getParameter("_error_message"));
 												}
 											}else{
 												out.print("请求被取消或页面没找到");
 											}
 										}
 									%>											
 									</font>
								<!--<td height="100%" width="100%">
           							<%          
           								pw =new PrintWriter(out);
         	 							// String isWriteErr = application.getInitParameter("isWriteErr");
           								if("true".equals(isWriteErr)&&request.getAttribute("MypmTestPermission")!=null){
               								if(ex!=null){
                   								if(ex instanceof BaseException ){
                   	  								if(((BaseException)ex).getLinkedException()!=null){
                   	  	 								((BaseException)ex).getLinkedException().printStackTrace(pw);
                   	  								}else{
                   	  									ex.printStackTrace(pw);
                   	  								}
                   								}else{
                   	  								ex.printStackTrace(pw);
                   								}
               								} else {
                       							if(exception!=null){
                       								exception.printStackTrace(pw);
                       							}
               								}
            							}
           							%>
         						</td>-->
								</td>
							</tr>
						</table>
					 </td>
			      </tr>
			</table>
		</div>
		
	</center>
</body>
<script type="text/javascript" language="javascript"> 
</script>
</HTML>
