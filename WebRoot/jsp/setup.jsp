<%@ page contentType="text/html; charset=UTF-8"%>
<%@taglib uri="/webwork" prefix="ww"%>
<%@taglib uri="/WEB-INF/pmButton.tld" prefix="pmTag"%>
<HTML>
<HEAD>
	<TITLE>初始化设置</TITLE>
	<META content="text/html; charset=UTF-8" http-equiv=Content-Type>
	<link rel="STYLESHEET" type="text/css" href="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxgrid.css">
	<script type="text/javascript" src="<%=request.getContextPath()%>/dhtmlx/grid/codebase/dhtmlxcommon.js"></script>
</HEAD>
<BODY style="overflow-y:hidden;">
<br><br><br><br><br><br><br><br>
<div id="setupDiv" class="gridbox_light" style="border:0px;display:none;z-index:99;">
	<div class="objbox" style="overflow:auto;width:100%;">
		<table class="obj row20px" cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr class="odd_mypm"><td>&nbsp;</td></tr>
			<tr class="odd_mypm"><td class="mypm_center"><img id="setupImgId" src="<%=request.getContextPath()%>/jsp/common/images/setup.gif" style="display:none;" /></tr>
			<tr class="odd_mypm"><td class="mypm_center"><div id="setupMessageText" ></div></td></tr>
			<tr class="odd_mypm"><td colspan="3" align="right" style="padding-right:8px;"><a class="bluebtn" href="javascript:void(0);" onclick="dbUpgrade()"
									style="margin-left: 6px"><span>升级</span> </a>&nbsp;&nbsp;</td></tr>
			<tr class="odd_mypm"><td>&nbsp;</td></tr>
		</table>
	</div>
</div>
<script type="text/javascript">
var setupW_ch = initW_ch(setupW_ch, "setupDiv", true, 380, 120, "", "MYPM数据库升级");
setupW_ch.button("close").hide();
function dbUpgrade(){
	$("setupImgId").style.display = "";
	$("setupMessageText").innerHTML = "正在初始化数据库...";
	var ajaxRest = dhtmlxAjax.postSync(conextPath + "/commonAction!initDatabases.action", "").xmlDoc.responseText;
	if (ajaxRest == "1") {
		$("setupMessageText").innerHTML = "<a href=# onclick=javascript:goLogin() >数据初始化成功 点击此链接进入登录页</a>";
		$("excuteInitImg").style.display = "none";
	}else if(ajaxRest == "duplicateUpgrade"){
		$("setupMessageText").innerHTML = "<a href=# onclick=javascript:goLogin() >数据之前已经初始化 无需再次初始化 点击此链接进入登录页</a>";
		$("excuteInitImg").style.display = "none";
	}else {
		$("setupMessageText").innerHTML = "数据库初始化失败";
	}
	$("setupImgId").style.display = "none";
}
function goLogin(){
	window.location = conextPath +"/jsp/userManager/login.jsp";
}
</script>
</BODY>
</HTML>