$(function(){
	$.parser.parse();
	//初始化图片大小
	initPicture();
	
	//自适应图片大小
	$(window).resize(function(){
		initPicture();
	});
	
	//点击进入研发任务管理
	$("#missionManage").click(function(){
		switchToOtherMission();
	});
	
	//点击项目迭代
	$("#iterationManage").click(function(){
		switchToDiedai();
	});
	//获取当前用户的权限
	getLoginUserPower();
});

//初始化图片大小
function initPicture(){
	var width = $("body").width() + 'px';
	var height = ($("body").height()-50) + 'px';
	
	$("#flowprocessPic").attr("width",width);
	$("#flowprocessPic").attr("height",height);
	
	//绘制热点坐标
	drawHotPoint();
}

//绘制热点坐标
function drawHotPoint(){
	var imgWidth = $('#flowprocessPic').width();
	var imgHeight = $('#flowprocessPic').height();
	
	//新建项目  热点坐标
	var createItemPos = Math.round(imgWidth*0.26207906295754024) + ',' + Math.round(imgHeight*0.16202945990180032) + ',' +Math.round( imgWidth * 0.43265007320644217 )+ ',' + Math.round(imgHeight * 0.23240589198036007);
    $('#createItem').attr('coords',createItemPos);
	
	//项目研发、缺陷追踪、测试执行、测试准备、测试分析的形状同高度
    var Height_Radis_Left = 0.4238952536824877;
    var Height_Radis_Right = 0.49427168576104746;
    
	//研发任务管理 热点坐标
	var missionManagePos = Math.round(imgWidth*0.09663250366032211) + ',' + Math.round(imgHeight*Height_Radis_Left) + ',' +Math.round( imgWidth * 0.21742313323572474 )+ ',' + Math.round(imgHeight * Height_Radis_Right);
    $('#missionManage').attr('coords',missionManagePos);
    
  //缺陷追踪管理 热点坐标
    var bugManagePos = Math.round(imgWidth*0.3103953147877013) + ',' + Math.round(imgHeight*Height_Radis_Left) + ',' +Math.round( imgWidth * 0.43191800878477304 )+ ',' + Math.round(imgHeight * Height_Radis_Right);
    $('#bugManage').attr('coords',bugManagePos);

    //测试用例管理
   	var testCaseManagePos = Math.round(imgWidth*0.44216691068814057) + ',' + Math.round(imgHeight*Height_Radis_Left) + ',' +Math.round( imgWidth * 0.5658857979502196 )+ ',' + Math.round(imgHeight * Height_Radis_Right);
   	$('#testCaseManage').attr('coords', testCaseManagePos);

   	 //设置测试流程
   	var testFlowPos = Math.round(imgWidth*0.5937042459736457) + ',' + Math.round(imgHeight*Height_Radis_Left) + ',' +Math.round( imgWidth * 0.7137628111273792 )+ ',' + Math.round(imgHeight * Height_Radis_Right);
   	$('#testFlow').attr('coords',testFlowPos);

   	 //测试需求管理
   	var testRequireManagePos = Math.round(imgWidth*0.7181551976573939) + ',' + Math.round(imgHeight*Height_Radis_Left) + ',' +Math.round( imgWidth * 0.8345534407027818 )+ ',' + Math.round(imgHeight * Height_Radis_Right);
    $('#testRequireManage').attr('coords',testRequireManagePos);

   	 //测试度量
   	var analysisManagePos = Math.round(imgWidth*0.8455344070278185) + ',' + Math.round(imgHeight*Height_Radis_Left) + ',' +Math.round( imgWidth * 0.9670571010248902 )+ ',' + Math.round(imgHeight * Height_Radis_Right);
   	$('#analysisManage').attr('coords', analysisManagePos);
   	
    //项目迭代
   	var iterationManagePos = Math.round(imgWidth*0.04685212298682284) + ',' + Math.round(imgHeight*0.8363338788870703) + ',' +Math.round( imgWidth * 0.5666178623718887 )+ ',' + Math.round(imgHeight * 0.9067103109656302);
   	$('#iterationManage').attr('coords', iterationManagePos);
}

//新建项目
/*function createItemWin(){
	 $("<div></div>").xdialog({
	    	id:'createItemWid',
	    	title:"权限查看",
	    	width : 400,
	        height : 450,
	    	modal:true,
	    	href:baseUrl + "/role/roleAction!browserAuth.action",
	    	queryParams:{'roleId' :row.roleId},
	        onClose : function() {
	            $(this).dialog('destroy');
	        }
	    });
}*/

//缺陷追踪管理
$("#bugManage").click(function(){
	if(privilegeMap["/bugManager/bugManagerAction!loadAllMyBug.action"]=="1"){
		bugTestCaseFlowDemandMan('bug');
	}else{
		tipPermission('请联系管理员，增加缺陷管理的权限！');
		return;
	}
});

//测试用例管理
$("#testCaseManage").click(function(){
	if(privilegeMap["/caseManager/caseManagerAction!loadCase.action"]=="1"){
		bugTestCaseFlowDemandMan('case');
	}else{
		tipPermission('请联系管理员，增加测试用例管理权限！');
		return;
	}
	
});

//设置测试流程
$("#testFlow").click(function(){
	if(privilegeMap["testTaskManagerAction!flwSetInit1"]!="1"){
		tipPermission('您目前还没有设置测试流程的权限！');
		return;
	}
	testFlowMan();
});

//测试需求管理
$("#testRequireManage").click(function(){
	if(privilegeMap["/outLineManager/outLineAction!index.action"]=="1"){
		bugTestCaseFlowDemandMan('require');
	}else{
		tipPermission('请联系管理员，增加测试需求管理权限！');
		return;
	}
});

//测试度量
$("#analysisManage").click(function(){
	if(privilegeMap["/analysis/analysisAction!goAnalysisMain.action"]=="1"){
		bugTestCaseFlowDemandMan('analy');
	}else{
		tipPermission('请联系管理员，增加测试度量管理权限！');
		return;
	}
	
});

function bugTestCaseFlowDemandMan(param){
	var urls = baseUrl + '/singleTestTask/singleTestTaskAction!swTestTaskList.action?dto.operCmd=caseList';
	if(!haveJoinProject()){
		tipPermission('您目前还没有参与任何测试项目，被设置到测试项目的流程中才被视为参与了该项目！');
		return;
	}
	$("#caseItemWin").xwindow('open');
	$("#caseItemWin").xwindow('setTitle','切换项目');
	$("#caseItemList").xdatagrid({
		url:urls,
		method: 'get',
		height: mainObjs.tableHeight,
		fitColumns:true,
		striped:false,
		singleSelect:true,
		checkOnSelect:true,
		emptyMsg:"暂无数据",
		scrollbarSize:100,
		pagination: true,
		pageNumber: 1,
		pageSize: 10,
		pageList: [15,30,50,100],
		layout:['list','first','prev','manual', "sep",'next','last','refresh','info'],
		rowStyler: function(index,row){
			if (row.status=='0'){
				
			}
		},
		onLoadSuccess:function(data){
			if(data.total>0){
				if($("#projectFlag").val()=="0"){
					$("#projectFlag").val("1");
					$("#menu").empty();
					loadLeftMenus();
				}
				
				$("#caseItemWin").window("vcenter");
			}

			//空循环1000次， 解有时候 resize 不好使的问题
			var i =1;
			for(;i<1000;i++){
				
			}
			$("#caseItemList").xdatagrid('resize');
		},
		onClickRow:function(rowIndex, rowData){
//			var dataU = $("#dataUrl").data('dataUrl');
			$("#leftMenu").show();
			$(".sidebary").show();
			$("#taksIdmain").val(rowData.taskId);
			$("#outlineState").val(rowData.outlineState);
		
			//分析度量
			//将分析度量页面数据放入session
			$("#analyitemId").val(rowData.taskId); 
			$("#analyprojectName").val(rowData.proName); 
			$("#analyproNum").val(rowData.proNum); 
			putAnalysisSession(rowData.taskId,rowData.proName,rowData.proNum);
			$("#addMenus").css('marginLeft','5em');
			
			if($('#leftMenu').width()=='75'){
				$('#menu li span').hide();
				mainObjs.$logoImg.attr("data-show","2");
			}else{
				mainObjs.$logoImg.attr("data-show","1");
			}
		
			$('#changePro').html("");
			if(rowData.proName.length > 3){
				$('#changePro').html("测试 > "+rowData.proName.substring(0,2)+"...");
			}else{
				$('#changePro').html("测试 > "+rowData.proName);
			}
			$('#changePro').attr("title",rowData.proName);
			$('#changePro').parent().prev().attr('src',baseUrl + '/itest/images/homeImage/switch1.png');
			$('#changePro').css('color','#ffffff');
			$(".l-menu").show();
			$(".content").removeClass('content_c').addClass('content');
			
			$("#menu").show();
			$("#otherMissionLeftMenu").hide();
			$("#caseTypeMenu").hide();
			$("#systemManage").css("color","#e9f6ff");
			$("#systemManage").html("设置");
			$("#otherMissionssss").css("color","#e9f6ff");
			$("#otherMissionImg").attr('src',baseUrl+'/itest/images/homeImage/elseTs.png');
			$("#systemM").css("margin","20px 0 -5px");
			
			$("#caseItemWin").xwindow('close');
			
			removeLeftMenuEffect();
			//切换系统管理的效果
			systemManX();
			//切换项目之后，跳转到具体的栏目中
			changeToProject(param);
			
			//每15分钟更新下session
			updateSession();
		},
		columns:[[
			{field:'proNum',title:'项目编号',width:'20%',align:'center'},
			{field:'proName',title:'项目名称',width:'30%',align:'left',halign:'center'},
			{field:'psmName',title:'项目PM',width:'25%',align:'left',halign:'center'},
			{field:'status',title:'状态',width:'10%',align:'center',formatter:status_Formats},
			{field:'testLdVos',title:'测试负责人',width:'28.4%',align:'left',formatter:tester_Format}
		]],
	});
}

//切换项目之后，跳转到具体的栏目中
function changeToProject(params){
	var jumpUrl = "";
	if(params == 'bug'){
		//缺陷管理
		$("#menu_107").attr('style','background: rgb(30, 124, 251);');
		$("#menu_107").find('img').removeAttr('src');
		$("#menu_107").find('img').attr('src',baseUrl+"/itest/images/homeImage/defect.png");
		$("#menu_107").find('span').css('color','#ffffff');
		
		jumpUrl = "/bugManager/bugManagerAction!loadAllMyBug.action";
	}else if(params == 'case'){
		$("#menu_87").attr('style','background: rgb(30, 124, 251);');
		$("#menu_87").find('img').removeAttr('src');
		$("#menu_87").find('img').attr('src',baseUrl+"/itest/images/homeImage/testSin.png");
		$("#menu_87").find('span').css('color','#ffffff');
		
		jumpUrl = "/caseManager/caseManagerAction!loadCase.action";
	}else if(params == 'require'){
		$("#menu_68").attr('style','background: rgb(30, 124, 251);');
		$("#menu_68").find('img').removeAttr('src');
		$("#menu_68").find('img').attr('src',baseUrl+"/itest/images/homeImage/projectMan.png");
		$("#menu_68").find('span').css('color','#ffffff');
		jumpUrl = "/outLineManager/outLineAction!index.action";
	}else{
		$("#menu_143").attr('style','background: rgb(30, 124, 251);');
		$("#menu_143").find('img').removeAttr('src');
		$("#menu_143").find('img').attr('src',baseUrl+"/itest/images/homeImage/analysis.png");
		$("#menu_143").find('span').css('color','#ffffff');
		jumpUrl = "/analysis/analysisAction!goAnalysisMain.action";
	}
//	setTimeout(function(){ 
		mainObjs.$wrapPage.load(baseUrl + jumpUrl);
//	},500);
}

function tester_Format(value,row,index) { 
	var html = new Array();
	if (row.testLdVos!=null&&row.testLdVos.length) {
		for (var i = 0; i < row.testLdVos.length; i++) {
			html.push(row.testLdVos[i].name);
		}
	}
	return html.join(',');
}

function status_Formats(value,row,index) {
	switch (value) {
	case 0:
		return '进行中';
	case 1:
		return '完成';
	case 2:
		return '结束';
	case 3:
		return '准备';
	case 5:
		return '暂停';
	case 6:
		return '终止';
	default:
		return '-';
	}
}
function testFlowMan(){
	var urls = baseUrl + '/singleTestTask/singleTestTaskAction!magrTaskListLoad.action';
	if(!haveJoinProjectTestFlow()){
		tipPermission('您目前还没有可选择的项目，请先新建项目！');
		return;
	}
	$("#caseItemWin").xwindow('open');
	$("#caseItemWin").xwindow('setTitle','选择项目');
	$("#caseItemList").xdatagrid({
		url:urls,
		method: 'get',
		height: mainObjs.tableHeight,
		fitColumns:true,
		striped:false,
		singleSelect:true,
		checkOnSelect:true,
		scrollbarSize:100,
		pagination: true,
		pageNumber: 1,
		pageSize: 10,
		pageList: [15,30,50,100],
		layout:['list','first','prev','manual', "sep",'next','last','refresh','info'],
		rowStyler: function(index,row){
			if (row.status=='0'){
				
			}
		},
		onLoadSuccess:function(data){
			if(data.total>0){
				$("#caseItemWin").window("vcenter");
			}
			//空循环1000次， 解有时候 resize 不好使的问题
			var i =1;
			for(;i<1000;i++){
				
			}
			$("#caseItemList").xdatagrid('resize');
		},
		onClickRow:function(rowIndex, rowData){
			$("#caseItemWin").xwindow('close');
			showTestFlowWin(rowData.taskId+"_"+rowData.taskType);
		},
		columns:[[
			{field:'proNum',title:'项目编号',width:'20%',align:'center'},
			{field:'proName',title:'项目名称',width:'30%',align:'left',halign:'center'},
			{field:'psmName',title:'项目PM',width:'25%',align:'left',halign:'center'},
			{field:'status',title:'状态',width:'10%',align:'center',formatter:status_Formats},
			{field:'testLdVos',title:'测试负责人',width:'28.4%',align:'left',formatter:tester_Format}
		]],
	});
}
function haveJoinProjectTestFlow(){
	$.ajaxSettings.async = false;
	var projectHaveFlag = false;
	$.post(baseUrl + '/singleTestTask/singleTestTaskAction!magrTaskListLoad.action',null,function(data){
		if(data.rows.length > 0){
			projectHaveFlag = true;
		}else{
			projectHaveFlag = false;
		}
	},'json');
	return projectHaveFlag;
}
//获取当前用户的权限
function getLoginUserPower(){
	var controlButton = $('button[schkUrl]');
	$.each(controlButton,function(i,n){
		var controId = controlButton[i].id;
		var controlUrl = $(controlButton[i]).attr('schkUrl');
		if(privilegeMap[controlUrl]!="1"){
			//$("#"+controlID).removeClass("hide"); 
//			$("[schkurl='"+schkUrl+"']").removeClass("hide");
			//$(":button[schkUrl]").removeClass("hide");  
			$("#"+controId).hide(); 
		}
	});
	var controlA = $('a[schkUrl]');
	$.each(controlA,function(i,n){
		var controId = controlA[i].id;
		var controlUrl = $(controlA[i]).attr('schkUrl');
		if(privilegeMap[controlUrl]!="1"){
			//$("#"+controlID).removeClass("hide"); 
//			$("[schkurl='"+schkUrl+"']").removeClass("hide");
			//$(":button[schkUrl]").removeClass("hide");  
			$("#"+controId).hide(); 
		}
	});
}
//# sourceURL=itestManual.js