var caseId, operaType, flag, testcasepkgId, testcasepkgName, computeExeCount, executeTaskId;
var fileInfos = new Array();
var currPagerObj =null,currPageSize =null,pagerNums =null,lastDataInd =null;
$(function() {
	exuiloader.load('numberbox', null, true);
	$.parser.parse();
	loadCaseCategory();
	loadCaseYXJ();

	caseId = getQueryParam('caseId');
	operaType = getQueryParam('operaType');
	flag = getQueryParam('flag');
	testcasepkgId = getQueryParam('testcasepkgId');
	testcasepkgName = getQueryParam('testcasepkgName');

	//迭代页面--【测试包管理】执行用例需要额外的传递参数
	executeTaskId = getQueryParam('executeTaskId');

	//用例包管理 页面传递的标识符 ：当用例执行成功后 标识是否计算已执行用例数。是：当用例第一次执行时计算。否：出现重新执行 已执行用例 的情况时，不计算执行用例数
	computeExeCount = getQueryParam('computeExeCount');
	editOrExecWindow();
	haveFileInfo(caseId, "caseEditFileLi", "editImg", "editCount");
	//点击基本信息隐藏历史列表
	$('a[href="#caseInfo"]').click(function() {
		$('#caseHistory').next().hide();
	});
	$('a[href="#caseEditFiles"]').click(function() {
		$('#caseHistory').next().hide();
	});

	if (operaType == "exec") {
		//选中列表第一页第一个行数据--隐藏【自动上一页】选项
		//当前选中行索引
		 selectedInd = $('#executeTestCaseList').datagrid('getRowIndex',$('#executeTestCaseList').datagrid('getSelected'));
	    //当前页码信息
		 currPagerObj = $("#executeTestCaseList").xdatagrid('getPager').data("pagination").options;
		 currPageNumber = currPagerObj.pageNumber;    //当前是第几页
		 currPageSize= currPagerObj.pageSize; //每页显示多少条数据
		 pagerNums = Math.ceil(currPagerObj.total/currPagerObj.pageSize); //分页总数
		 var lastPageDataNum = currPagerObj.total%currPagerObj.pageSize; //最后一页显示多少条数据
		 lastDataInd =  (lastPageDataNum == 0)? currPagerObj.pageSize-1:lastPageDataNum-1 ;   //最后一页最后一条数据的索引
		 //判断是否显示上一个、下一个、自动上一个、自动下一个按钮
		 showPreviousAndNextCaseBtn();
	}
	
});

function showPreviousAndNextCaseBtn(){
	if(selectedInd == 0 && currPageNumber == 1){
		$('#autoPrevious').prop('checked',false);
       $('#previousWrapper').css('display','none');
       $('#previousBtn').css('display','none');
       
       $('#nextWrapper').css('display','inline-block');
	   $('#nextBtn').css('display','inline-block');
	   $('#autoNext').prop('checked',true);
    }else if(currPageNumber == pagerNums && lastDataInd == selectedInd){
    	$('#autoNext').prop('checked',false) 
    	$('#nextWrapper').css('display','none');
    	$('#nextBtn').css('display','none');
    	
    	$('#previousWrapper').css('display','inline-block');
        $('#previousBtn').css('display','inline-block');
        $('#autoPrevious').prop('checked',true);
    }else{
    	$('#previousWrapper').css('display','inline-block');
        $('#previousBtn').css('display','inline-block');
        
        $('#nextWrapper').css('display','inline-block');
 	    $('#nextBtn').css('display','inline-block');
 		var autoLoadDataFlag = $('#execDiv input:radio:checked').val();
 		if(!autoLoadDataFlag){ 			
 			$('#autoNext').prop('checked',true);
 		}
    }
}

//用例历史
function caseHistory() {
	$("#caseHistoryList").xdatagrid({
		url: baseUrl + '/caseManager/caseManagerAction!viewCaseHistory.action?dto.testCaseInfo.testCaseId=' + caseId,
		method: 'get',
		height: mainObjs.tableHeight - 100,
		fitColumns: true,
		singleSelect: true,
		pagination: true,
		pageNumber: 1,
		pageSize: 16,
		pageList: [16, 30, 50, 100],
		layout: ['list', 'first', 'prev', 'manual', "sep", 'next', 'last', 'refresh', 'info'],
		onLoadSuccess: function(data) {
			$("#caseHistoryList").xdatagrid('resize');
		},
		columns: [
			[{
				field: 'testActorName',
				title: '处理人',
				width: '15%',
				align: 'center',
				formatter: function(value, row, index) {
					if (value == "" || value == "null" || value == null) {
						return " ";
					} else {
						return "<p title=\"" + value + "\">" + value + "</p>";
					}
				}
			}, {
				field: 'operaType',
				title: '操作类型',
				width: '10%',
				align: 'center',
				formatter: operaTypeFormat
			}, {
				field: 'testResult',
				title: '用例状态',
				width: '10%',
				align: 'center',
				formatter: convCase
			}, {
				field: 'testVerNum',
				title: '版本',
				width: '15%',
				align: 'center',
			}, {
				field: 'exeDate',
				title: '处理日期',
				width: '25%',
				align: 'center'
			}, {
				field: 'remark',
				title: '备注',
				width: '25%',
				align: 'center',
				formatter: function(value, row, index) {
					if (value == "" || value == "null" || value == null) {
						return "";
					} else {
						return "<p title=\"" + value + "\">" + value + "</p>";
					}
				}
			}]
		]
	});
	$("#caseHistoryList").xdatagrid('resize');
}

//获取页面url参数
function getQueryParam(name) {
	var obj = $('#editCaseWindown').xwindow('options');
	var queryParams = obj["queryParams"];
	return queryParams[name];
}

//编辑
function editOrExecWindow() {
	$('#caseHistory').next().hide();
	$("#editCaseForm").xform('clear');
	$("#caseWeight").numberbox("setValue", 2);
	var url = "";
	if (operaType == "exec") {
		url = baseUrl + "/caseManager/caseManagerAction!exeCaseInit.action";
	} else {
		url = baseUrl + "/caseManager/caseManagerAction!upInit.action";
	}
	var objParam = {};
	if (executeTaskId) {
		objParam = {
			'dto.testCaseInfo.testCaseId': caseId,
			'dto.taskId': executeTaskId
		}
	} else {
		objParam = {
			'dto.testCaseInfo.testCaseId': caseId
		}
	}

	$.get(url,
		objParam,
		function(data, status, xhr) {
			if (status == 'success') {
				$("#editCaseForm").xdeserialize(data);
			} else {
				$.xalert({
					title: '提示',
					msg: '系统错误！'
				});
			}
		}, "json");
	$('#submit_1').linkbutton({
		text: '修改'
	});
	$("#execTr").hide();
	$("#addOrEditFoot").show();
	$("#operaDiv").show();
	$("#execDiv").hide();
	if (operaType == "exec") {
		$("#exeVerId").xcombobox({
			url: baseUrl + '/testTaskManager/testTaskManagerAction!loadVerSel.action',
			valueField: 'keyObj',
			textField: 'valueObj',
			onSelect: function(rec) {
				executeVersion = rec.keyObj;
			}
		});
		$("#execTr").show();
		$("#addOrEditFoot").show();
		$("#operaDiv").hide();
		$("#execDiv").show();
		if(executeVersion){
			$("#exeVerId").xcombobox('setValue',executeVersion);
		}
	}
	//	$("#editCaseWindown")[0].style.maxHeight=mainObjs.tableHeight-40;
	//	$("#editCaseWindown").window({title:"修改用例"});
	$("#editCaseWindown").window("vcenter");
	//	$("#editCaseWindown").window("open");

}

//提交保存新增或修改
function editSubmit(submitId) {
	var valid = $("#editCaseForm").xform('validate');
	if (!valid) {
		return;
	}
	//保存不选择版本号

	if (submitId != "2") {		
		$('#submit_' + submitId).linkbutton('disable');
	}
	var testCaseId = $("#testCaseId").val();
	var url = baseUrl + "/caseManager/caseManagerAction!upCase.action"
	$.post(
		url,
		$("#editCaseForm").xserialize(),
		//		dataParams,
		function(data) {
			if (data == "success") {
				$('#submit_1').linkbutton('enable');
				if (testCaseId == "") {
					closeEditCaseWin();
					if (flag === 'testCase') {
						$("#executeTestCaseList").xdatagrid('reload');
					} else {
						getCaseList(currNodeId);
					}
					$.xalert({
						title: '提示',
						msg: '增加成功！'
					});
					
				} else {
					if (submitId == "2") {
						$("#executeTestCaseList").xdatagrid('reload');
						$("#executeTestCaseList").xdatagrid('gotoPage',currPageNumber);
			        	$("#executeTestCaseList").xdatagrid('selectRow',selectedInd);
						$.xalert({
							title: '提示',
							msg: '保存成功！'
						});
					} else {
						closeEditCaseWin();
						if (flag === 'testCase') {
							$("#executeTestCaseList").xdatagrid('reload');
						} else {
							getCaseList(currNodeId);
						}
						$.xalert({
							title: '提示',
							msg: '修改成功！'
						});
					}
				}

			} else {
				
				$('#submit_' + submitId).linkbutton('enable');
				$.xalert({
					title: '提示',
					msg: '系统错误！'
				});
			}
		}, "text");
}

function closeEditCaseWin() {
	//取消
	$("#editCaseForm").xform('clear');
	$("#editCaseWindown").xwindow('close');
	
}


//执行用例提交
function execCaseSubmit(status) {
	var valid = $("#editCaseForm").xform('validate');
	var autoLoadDataFlag = $('#execDiv input:radio:checked').val();
	if (!valid) {
		return;
	}
	if ($("#exeVerId").xcombobox("getValue") == "") {
		$.xalert({
			title: '提示',
			msg: '请选择版本！'
		});
		return;
	}
	
//	$('#exec_' + status).linkbutton('disable');
	var url = baseUrl + "/caseManager/caseManagerAction!exeCase.action";
	var params = {
		'dto.testCaseInfo.testCaseId': $("#testCaseId").val(),
		'dto.testCaseInfo.moduleId': $("#moduleId").val(),
		'dto.testCaseInfo.testStatus': status,
		'dto.testCasePackId': testcasepkgId,
		'dto.testcasepkgName': testcasepkgName,
		'dto.exeVerId': $("#exeVerId").val(),
		'dto.remark': $("#execRemark").val(),
		'dto.fileInfos': JSON.stringify(fileInfos),
		'dto.computeExeCountFlag': computeExeCount //执行用例成功  重新计算执行数---用例包管理
	};
	$.post(url, params, function(data) {
		var exeVersionId = $("#exeVerId").xcombobox("getValue");
		if(!executeVersion){
			executeVersion = exeVersionId;//存储版本号--自动回填下一个执行用例弹框
		}
		if (data == "success") {
			$.xalert({
				title: '提示',
				msg: '执行成功！'
			});
			if(autoLoadDataFlag){
				if(autoLoadDataFlag == 'previous'){
					getPreviousCaseData();
				}else if(autoLoadDataFlag == 'next'){
					getNextCaseData();	
				}
			}else{
			//	$("#editCaseWindown").xwindow('close');
				if (flag === 'testCase') {
//					$("#executeTestCaseList").xdatagrid('reload');
				} else {
					getCaseList(currNodeId);
				}
//				$('#exec_' + status).linkbutton('enable');
			}
           
		} else {
//			$('#exec_' + status).linkbutton('enable');
			$.xalert({
				title: '提示',
				msg: '系统错误！'
			});
		}
	}, "text");

}

//加载前一条数据
function getPreviousCaseData(){

	var rowData = null,testCaseId=null;
	//共1页数据
	if(pagerNums == 1){
		//第一页第一条数据--提示
		if(selectedInd == 0){
			handlePreviousFirstRow()	
		}else{
			//其他行数据
			handlePreviousOtherRow();
		}
	}else{
		//第一页
		if(currPageNumber == 1){
			//第一页第一条数据--提示
			if(selectedInd == 0){
				handlePreviousFirstRow()	
			}else{
				//其他行数据
				handlePreviousOtherRow();
			}
		}else{
			//其他页第一条数据--翻页
			if(selectedInd == 0){
				handlePreviousOtherPagesFirstRow();
			}else{
				//其他行数据
				handlePreviousOtherRow();
			}
		}
	}
	 //判断是否显示上一个、下一个、自动上一个、自动下一个按钮
	 showPreviousAndNextCaseBtn();
}

//当选中是第一页第一条数据时--提示
function handlePreviousFirstRow(){
	var rowData = null,testCaseId=null;
    $.xalert({
		title: '提示',
		msg: '这是第一条数据！'
	});
}

//当选中的是中间数据时--加载上一条数据
function handlePreviousOtherRow(){
	var rowData = null,testCaseId=null;
	selectedInd = selectedInd-1;
	//选中上一条数据
	 $("#executeTestCaseList").xdatagrid('selectRow',selectedInd);
	 rowData = $("#executeTestCaseList").xdatagrid('getSelected');

	 caseId = rowData.testCaseId;
	getTestCaseInfo(caseId);
	haveFileInfo(caseId, "caseEditFileLi", "editImg", "editCount");
}

//当选中的除首页之外的第一条数据
function handlePreviousOtherPagesFirstRow(){
	var rowData = null,testCaseId=null;
	//翻上一页 最后一条数据
	currPageNumber = currPageNumber-1;
	$("#executeTestCaseList").xdatagrid('gotoPage',currPageNumber);
	selectedInd = currPageSize-1;
	$("#executeTestCaseList").xdatagrid('selectRow',selectedInd);
	rowData = $("#executeTestCaseList").xdatagrid('getSelected');

	caseId = rowData.testCaseId;
	getTestCaseInfo(caseId);
	haveFileInfo(caseId, "caseEditFileLi", "editImg", "editCount");
}

function getNextCaseData(){
	var rowData = null,testCaseId=null;
	if(pagerNums == 1){
		//最后一条数据
		if(selectedInd == lastDataInd){
			handleNextLastRow();
		}else{
			handleNextOtherRow();
		}
	}else{
		if(currPageNumber == pagerNums){
			if(selectedInd == lastDataInd){
				handleNextLastRow();
			}else{
				handleNextOtherRow();
			}
		}else{
			if(selectedInd == (currPageSize-1) ){
				handleNextOtherPagesLastRow();
			}else{
				handleNextOtherRow();
			}
		}
	}
	 //判断是否显示上一个、下一个、自动上一个、自动下一个按钮
	 showPreviousAndNextCaseBtn();
}

//当选中是最后一页最后一条数据时--提示
function handleNextLastRow(){

    $.xalert({
		title: '提示',
		msg: '这是最后一条数据！'
	});
}

function handleNextOtherRow(){
	//下一条数据
	selectedInd = selectedInd + 1;
	 $("#executeTestCaseList").xdatagrid('selectRow',selectedInd);
	 
	 rowData = $("#executeTestCaseList").xdatagrid('getSelected');
	
	 caseId = rowData.testCaseId;
  	 getTestCaseInfo(caseId);
  	haveFileInfo(caseId, "caseEditFileLi", "editImg", "editCount");
}

//翻页
function handleNextOtherPagesLastRow(){
	currPageNumber = currPageNumber + 1;
	$("#executeTestCaseList").xdatagrid('gotoPage',currPageNumber);
	selectedInd = 0;
	$("#executeTestCaseList").xdatagrid('selectRow',selectedInd);
	rowData = $("#executeTestCaseList").xdatagrid('getSelected');
	caseId = rowData.testCaseId;
	getTestCaseInfo(caseId);
	haveFileInfo(caseId, "caseEditFileLi", "editImg", "editCount");
}

function getTestCaseInfo(testCaseId){	
	resetOptBtn();
	
	$("#editCaseForm").xform('clear');	
	var url = baseUrl + "/caseManager/caseManagerAction!exeCaseInit.action";
	
	var objParam = {};
	if (executeTaskId) {
		objParam = {
			'dto.testCaseInfo.testCaseId': testCaseId,
			'dto.taskId': executeTaskId
		}

	} else {
		objParam = {
			'dto.testCaseInfo.testCaseId': testCaseId
		}
	}
	$.get(url,
			objParam,
			function(data, status, xhr) {
				if (status == 'success') {
					$("#editCaseForm").xdeserialize(data);
					if(executeVersion){						
						//如有版本缓存，则回填
						$("#exeVerId").xcombobox("setValue",executeVersion)
					}
					var exeStatus = data['dto.testCaseInfo'].testStatus;
					 //当用例未执行时  设置标识符标识需要重新计算执行用例数
					 if(exeStatus == "" || exeStatus == 1 || exeStatus == null){
						 computeExeCount = "computeExeCount";
					 }else{
						 computeExeCount = "";
					 } 
				} else {
					$.xalert({
						title: '提示',
						msg: '系统错误！'
					});
				}
			}, "json");
}

function resetOptBtn(){
	//删除按钮不可用属性
	$('#submit_2').linkbutton('enable');
	$('#exec_2').linkbutton('enable');
	$('#exec_3').linkbutton('enable');
	$('#exec_4').linkbutton('enable');
	$('#exec_5').linkbutton('enable');
	
	$('#submit_2').removeClass('active');
	$('#exec_2').removeClass('active');
	$('#exec_3').removeClass('active');
	$('#exec_4').removeClass('active');
	$('#exec_5').removeClass('active');
	
	$('#nextBtn').removeClass('active');
    $('#previousBtn').removeClass('active');
}

//文件信息
function editCaseFile() {
	$('#caseHistory').next().hide();
	$.get(
		baseUrl + "/fileInfo/fileInfoAction!getFileInfoByTypeId.action", {
			'dto.fileInfo.type': 'case',
			'dto.fileInfo.typeId': caseId
		},
		function(data, status, xhr) {
			if (status == 'success') {
				var fileData = new Array();
				var preConfigList = new Array();
				for (var i in data) {
					fileData.push(baseUrl + data[i].filePath);
					var index = data[i].relativeName.lastIndexOf(".");
					var fileTypeName = data[i].relativeName.substring(index + 1, data[i].relativeName.length);
					var configJson = {
						showDownload: true,
						showRemove: true,
						downloadUrl: baseUrl + data[i].filePath,
						caption: data[i].relativeName, // 展示的文件名 
						width: '120px',
						url: baseUrl + "/fileInfo/fileInfoAction!delteById.action", // 删除url 
						key: data[i].fileId, // 删除是Ajax向后台传递的参数 
						extra: {
							"dto.fileInfo.fileId": data[i].fileId
						}
					};

					if (fileTypeName == "text") {
						var addParam = {
							type: 'text'
						};
						Object.assign(configJson, addParam);
					}
					if (fileTypeName == "pdf") {
						var addParam = {
							type: 'pdf'
						};
						Object.assign(configJson, addParam);
					}
					if (fileTypeName == "doc" || fileTypeName == "docx") {
						var addParam = {
							type: 'gdocs'
						};
						Object.assign(configJson, addParam);
					}
					if (fileTypeName == "mp4" || fileTypeName == "avi" || fileTypeName == "mov" || fileTypeName == "gif" || fileTypeName == "wmv") {
						var addParam = {
							type: 'video',
							filetype: "video/mp4"
						};
						Object.assign(configJson, addParam);
					}
					preConfigList.push(configJson);
				}
				initCaseFileInput(fileData, preConfigList);
			} else {
				$.xalert({
					title: '提示',
					msg: '系统错误！'
				});
			}
		}, "json");
}

var uploadCount = 0;

function initCaseFileInput(fileJson, fileConfigJson) {
	$("#caseEditFileId").fileinput('clear');
	$("#caseEditFileId").fileinput('destroy');
	$("#caseEditFileId").fileinput({
		theme: 'fa',
		language: 'zh',
		uploadUrl: baseUrl + "/uploader?type=case",
		//		allowedFileExtensions: ['jpg', 'png', 'gif','bmp','ppt','pptx','txt','pdf','doc','docx','object','gdocs'],
		showPreview: true,
		showClose: false,
		overwriteInitial: false,
		uploadAsync: false,
		//		autoReplace: true,
		showUploadedThumbs: true,
		maxFileSize: 6000,
		maxFileCount: 5,
		imageMaxWidth: 200,
		imageMaxHeight: 100,
		enctype: 'multipart/form-data',
		//msgFilesTooMany :"选择图片超过了最大数量", 
		showRemove: false,
		showClose: false,
		showUpload: true,
		showDownload: true,
		allowedPreviewTypes: ['image', 'html', 'text', 'video', 'pdf', 'flash', 'object'],
		dropZoneEnabled: false,
		fileActionSettings: {
			showUpload: false,
			showDownload: true,
		}, // 控制上传按钮是否显示
		initialPreviewAsData: true,
		initialPreview: fileJson,
		initialPreviewConfig: fileConfigJson,
		slugCallback: function(filename) {
			return filename.replace('(', '_').replace(']', '_');
		}
	}).on("fileselect", function(event, files) {
		//选择文件
		var filesCount = $('#caseEditFileId').fileinput('getFilesCount');
		uploadCount = 0;
	}).on("filebatchselected", function(event, files) {
		//选择文件后处理
	}).on('filebatchuploadsuccess', function(event, data, previewId, index) {
		if (uploadCount == 0) {
			var fInfos = data.response;
			var params = {
				"dto.type": "case",
				"dto.typeId": caseId,
				"dto.fileInfos": JSON.stringify(fInfos)
			};
			uploadFileInfo(params);
			uploadCount = 1;
			setTimeout(function() {
				$(".kv-upload-progress").hide();
			}, 2000);
		}
	}).on('fileloaded', function(event, data, previewId, index) {
		var filesFrames = $('#caseEditFileId').fileinput('getFrames');
		if (filesFrames.length > 5) {
			$.xalert({
				title: '提示',
				msg: '最多上传5个文件！'
			});
			$("#" + previewId).remove();
			return;
		}
	}).on('filebatchuploaderror', function(event, data, previewId, index) {}).on('filepreupload', function(event, data, previewId, index) {}).on('fileerror', function(event, data, msg) {}).on("fileuploaded", function(event, data, previewId, index) {
		//上传成功后处理方法
	}).on('filedeleted', function(event, index) {
		haveFileInfo(caseId, "caseEditFileLi", "editImg", "editCount");
	});
	$(".file-caption-name").attr('placeholder', '点击后粘贴截屏或选择文件...');
}

//文件上传
function uploadFileInfo(params) {
	$.post(
		baseUrl + "/fileInfo/fileInfoAction!addFileInfo.action",
		params,
		function(data) {
			if (data == "success") {
				editCaseFile();
				$('#caseEditFileId').fileinput('enable');
				$(".btn-file").removeAttr("disabled");
				haveFileInfo(caseId, "caseEditFileLi", "editImg", "editCount");
				$.xalert({
					title: '提示',
					msg: '附件上传成功！'
				});

			} else {
				$.xalert({
					title: '提示',
					msg: '附件上传失败！'
				});
			}
		}, "json");
}

//判断是否有文件
function haveFileInfo(caseId, liId, imgId, fileCountId) {
	$.get(
		baseUrl + "/fileInfo/fileInfoAction!getFileInfoByTypeId.action", {
			'dto.fileInfo.type': 'case',
			'dto.fileInfo.typeId': caseId
		},
		function(data, status, xhr) {
			if (status == 'success') {
				if (data != null && data.length > 0) {
					$("#" + imgId).show();
					$("#" + fileCountId).html(data.length);
					$("#" + liId).mouseenter(function() {
						$("#" + fileCountId).show();
					});
					$("#" + liId).mouseleave(function() {
						$("#" + fileCountId).hide();
					});
				} else {
					$("#" + imgId).hide();
					$("#" + fileCountId).hide();
					$("#" + liId).unbind('mouseenter').unbind('mouseleave');
				}

			} else {
				$("#" + imgId).hide();
				$("#" + fileCountId).hide();
				$("#" + liId).unbind('mouseenter').unbind('mouseleave');
			}
		}, "json");
}

//# sourceURL=editCase.js