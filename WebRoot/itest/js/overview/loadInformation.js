$(function() {
	$.parser.parse();
	//得到所有统计信息
	loadInformation();
	bugWorkInfo();
});
function loadInformation(){
	$.post(baseUrl+"/overview/overviewAction!getDetails.action",{
		'dto.taskId':$("#taksIdmain").val(),
		'dto.chargePersonId':$("#accountId").text(),
		'dto.createId':$("#loginNam").text(),
		'dto.joinId':$("#accountId").text()
	},function(data){
		$("#11").html(data.allCount);
		$("#22").html(data.passCount);
		$("#33").html(data.failedCount);
		$("#44").html(data.blockCount);
		$("#55").html(data.noTestCount);
		$("#66").html(data.invalidCount);
		$("#77").html(data.casePkgCount);
		$("#88").html(data.exeRatio.split("/")[0]);
		$("#99").html(data.exeRatio.split("/")[1]);
		$("#aa").html(data.bugAllCount);
		$("#bb").html(data.bugValidCout);
		$("#cc").html(data.bugResolCount);
		$("#dd").html(data.bugValidCout - data.bugResolCount);
		$("#ee").html(data.meCharge);
		$("#ff").html(data.meCreate);
		$("#gg").html(data.meJoin);
		$("#hh").html(data.meAll);
		$("#fixCount").html(data.fixNoConfirmCount);
		$("#noBugCount").html(data.noBugNoConfirmCount);
	},'json');
}
//测试人员工作面板
function bugWorkInfo(){
	$("#bugWorkList").xdatagrid({
		url: baseUrl + '/caseManager/caseManagerAction!loadCaseBoard.action',
		method: 'get',
		fitColumns:true,
		singleSelect:true,
		/*pagination: true,*/
		/*pageNumber: 1,
		pageSize: 10,*/
		height: mainObjs.tableHeight - 260,
		/*pageList: [10,30,50,100],
		layout:['list','first','prev','manual', "sep",'next','last','refresh','info'],*/
		onLoadSuccess:function(data){
			$("#bugWorkList").xdatagrid('resize');
		},
		rowStyler: function(index,row){
			if (row.status=='0'){
//				return 'background-color:#bddecc;color:#fff;font-weight:bold;';
//				return 'background-color:#e5fff1;';
			}
		},
		//whCount 处理用例数 ，hCount 今日处理用例数
		columns:[[
			{field:'userName',title:'项目人员',width:'14%',align:'center'},
			{field:'teamActor',title:'项目分工',width:'14%',align:'center'},
			{field:'hcount',title:'今日处理用例数',width:'18%',align:'center'},
			{field:'whCount',title:'处理用例数',width:'18%',align:'center'},
			{field:'bhCount',title:'今日处理bug次数',width:'18%',align:'center'},
			{field:'bwhCount',title:'待处理bug数',width:'18%',align:'center',formatter:bwhCountFormat}
			
		]]
	});
	$("#bugWorkList").xdatagrid('resize');
}

function bwhCountFormat(value,row,index){
	if(value > 0){
		return "<a onclick='jumpToBug("+JSON.stringify(row)+")' style='cursor:pointer;' title='查看此人待处理Bug'>"+value+"</a>";
	}else{
		return value;
	}
}
function jumpToBug(row){
	if($("body").find("li[id='menu_107']").length > 0){
		jumpToBugPage('menu_107','107',row.teamActor,row.userId,row.loginName+"("+row.userName+")");
	}else{
		$.xalert({title:'提示',msg:'您目前没有缺陷管理的权限，请联系管理员维护！'});
	}
}
//跳转到缺陷管理模块，带上相应条件进行查询
function jumpToBugPage(id,dataInfo,teamActor,userId) {
	var id = id;
	var functionId = dataInfo;
	var url = mainObjs.$menu.data('menus')[id] ? mainObjs.$menu.data('menus')[id].url : null;
	$("#dataUrl").data("dataUrl",id+"^"+$("#"+id).find('img').attr('src')+"^"+url);
	
	if($("#outlineState").val()!="1"){//未提交测试需求
		if(id=="menu_107"||id=="menu_87"||id=="menu_143"||id=="menu_11"){
			var homepa_s = "";
			var clickData = $("#dataUrl").data("dataUrl").split("^");
			if($("#myhome").data("myHome")!=undefined){
				homepa_s = $("#myhome").data("myHome").split("&");
				if(homepa_s[0]==clickData[2]){
					$("#"+clickData[0]).css('background','rgb(30, 124, 251)');
					$("#"+clickData[0]).find('img').attr('src',baseUrl + '/itest/images/homeImage/defect.png');
					$("#"+clickData[0]).find('span').css('color','#ffffff');
				}
				$.xalert({title:'提示',msg:'因测试需求没提交，请提交测试需求!'});
				return;
			}else{
				$.xalert({title:'提示',msg:'因测试需求没提交，请提交测试需求!'});
				return;
			}
		}
	}
	
	
		$("#"+id).css('background','rgb(30, 124, 251)');
		$("#"+id).find('img').attr('src',baseUrl + '/itest/images/homeImage/defect.png');
		$("#"+id).find('span').css('color','#ffffff');
		$("#adds").data('sty',0);
		if(id=="menu_s"){
			setSelfHomePage();
		}
		
		var allLi = mainObjs.$menu.find('li');
		$.each(allLi,function(i,n){
			var firstD ="";
			var currPic = $("#"+allLi[i].id).find('a > img').attr('src');
			var picPath = currPic.split(".png");
			if($("#"+allLi[i].id).attr('style')!=undefined){
				firstD = $("#"+allLi[i].id).attr('style').indexOf('background: rgb(30, 124, 251)');
				if(firstD>=0){
					if(id!=allLi[i].id){
						$("#"+allLi[i].id).attr('style','');
						$("#"+allLi[i].id).find('img').attr('src',picPath[0]+"1.png");
						$("#"+allLi[i].id).find('span').css('color','#E9F6FF');
					}
				$("#adds").data('sty',1);
				}
			}
			
		});		
		
	if(url=="/userManager/userManagerAction!setMyInfoInit.action"){
		showAddWin01();
		return;
	}
	if (url) {
		destoryEle();
		/*if($("#accountId").text() != userId){*/
			localStorage.teamActor = teamActor;
			localStorage.userId = userId;
			//localStorage.name = name;
		/*}*/
		mainObjs.$wrapPage.load(baseUrl + url);
	}
}
//# sourceURL=loadInformation.js