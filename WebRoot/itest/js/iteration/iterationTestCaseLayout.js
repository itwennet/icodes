var iterationId="";
var taskId ="";
var currNodeId="";
var rootNodeId;
var switchFlag=0;
var loadCount=0;
var packageIds="";

//获取页面url参数
function getQueryParam(name) {
       var obj = $('#iterationTestCaseLayout').xdialog('options');
       var queryParams = obj["queryParams"]; 
       return queryParams[name];
}

$(function(){
	$.parser.parse();
	iterationId = getQueryParam('iterationListId');
	taskId = getQueryParam('taskBugId');
	packageIds = getQueryParam('testCaseData');
//	caseTree();
	getTestcaseDetails(taskId);
	loadExecPeopleLists();
});

//测试包的详细
function getTestcaseDetails(){
	$("#TestCaseList").xdatagrid({
		url:  baseUrl + '/testCasePkgManager/testCasePackageAction!loadTestCasePackageList.action',
		method: 'post',
		height: mainObjs.tableHeight-140,
		queryParams:{
						'dto.testCasePackage.taskId':taskId,
						'dto.packageIdJoin':packageIds,
						'dto.selectedUserIds':'all'
					},
		striped:false,
		pagination: true,
		fitColumns: true,
		rownumbers: true,
		multiple:true,
		emptyMsg:"暂无数据",
		pageNumber: 1,
		pageSize: 10,
		layout:['list','first','prev','manual','next','last','refresh','info'],
		columns:[[
		    {field:'checkId',title:'选择',checkbox:true,align:'center'},
			{field:'packageId',hidden:true},
			{field:'packageName',title:'测试包名称',width:'25%',height:'50px',align:'center',halign:'center'},
			{field:'execEnvironment',title:'执行环境',width:'25%',height:'50px',align:'center'},
			{field:'executor',title:'执行人',width:'25%',height:'50px',align:'center',halign:'center'},
			{field:'remark',title:'备注',width:'25%',height:'50px',align:'center'},
//			{field:'operator',title:'操作',width:'20%',height:'50px',align:'center',formatter:operatFormat},
			{field:'testCaseNames',hidden:true}
		]],
		onLoadSuccess : function (data) {								
			if (data.total==0) {
//				var contentTestCase = $('div[class="tab-content"]').children().eq(2).find($('tr[id^="datagrid-row-r"]'));
//				contentTestCase.empty();
//				contentTestCase.append('<label style="height: 40px;width: 900px;margin-top: 8px;margin-left: 16%;text-align: center;">暂无数据!</label>');
			}
		}
	});
}





function submitTestCase(){
	var selItems = 	$("#TestCaseList").xdatagrid('getSelections');
	if(selItems.length <= 0){
//		$.xalert({title:'提示',msg:'请选择测试用例！'});
		tip("请选择测试用例！");
		return;
	}else{
		selTestCaseIds = selItems.map(function(value){
			return value.packageId;
		});
	}
	
	$.post(
			baseUrl + "/iteration/iterationAction!saveTestCasePackage.action",
			{"dto.iterationList.iterationId": iterationId,
			 "dto.testCaseP":selTestCaseIds.toString()},
			function(dataObj) {
				if(dataObj.indexOf("success") === -1){
//					$.xalert({title:'提示',msg:'保存失败，请稍后再试！'});
					tip("保存失败，请稍后再试！");
				}else{
					$.xalert({title:'提示',msg:'保存成功!',okFn:function(){
						$('#iterationTestCaseLayout').dialog('destroy');
						getTestcaseList();//获取得到关联的测试包
					}});
//					tipSave();
					
				}
			},"text"
		);
}

function tipSave(){
	$.xconfirm({
		msg:"保存成功!",
		okFn: function() {
			$('#iterationTestCaseLayout').dialog('destroy');
			getTestcaseList();//获取得到关联的测试包
		}
	});
}

function getTestcaseList(){
	objs.$proTestcase.xdatagrid({
		url:  baseUrl + '/iteration/iterationAction!searchTestCaseDetail.action',
		method: 'post',
		height: mainObjs.tableHeight-140,
		queryParams:{'dto.iterationList.iterationId':iterationId},
		striped:false,
		pagination: true,
		fitColumns: true,
		rownumbers: true,
		multiple:true,
		emptyMsg:"暂无数据",
		pageNumber: 1,
		pageSize: 10,
		layout:['list','first','prev','manual','next','last','refresh','info'],
		columns:[[
	        {field:'checkId',title:'选择',checkbox:true,align:'center'},
			{field:'packageId',hidden:true},
			{field:'packageName',title:'测试包名称',width:'20%',height:'50px',align:'center',halign:'center'},
			{field:'execEnvironment',title:'执行环境',width:'20%',height:'50px',align:'center'},
			{field:'executor',title:'执行人',width:'20%',height:'50px',align:'center',halign:'center'},
			{field:'remark',title:'备注',width:'20%',height:'50px',align:'center'},
			{field:'exeCount',title:'执行率',width:'5%',height:'50px',align:'center',formatter:function(value,row,index){
                if(!value){
                	value=0
                }
                if(!row.notExeCount){
                	row.notExeCount=0
                }
                
                sumCase = value + row.notExeCount;
                
                if(!sumCase){
                	return '-'
                }
				return value + '/' + sumCase
		     }},
			{field:'operator',title:'操作',width:'20%',height:'50px',align:'center',formatter:operat_Format},
			{field:'testCaseNames',hidden:true}
		]],
		onLoadSuccess : function (data) {
			var packageId_join="";
			if (data.total==0) {
//				var contentTestCase = $('div[class="tab-content"]').children().eq(2).find($('tr[id^="datagrid-row-r"]'));
//				contentTestCase.empty();
//				contentTestCase.append('<label style="height: 40px;width: 900px;margin-top: 8px;margin-left: 16%;text-align: center;">暂无数据!</label>');
			}else{
				for(var i=0; i<data["rows"].length;i++){
					packageId_join = packageId_join+data.rows[i].packageId+" ";
				}
				$("#packageIds").val($.trim(packageId_join));
			}
			if(windowHeight>899){
				var boxHeight = windowHeight*0.6;
				$('.tab-content > div[class="panel panel-default datagrid panel-htop"]').height(boxHeight);
				$('#proTestcase').prev().find($('div[class="datagrid-body"]')).height(boxHeight-90);
				$('.tab-content > div[class="panel panel-default datagrid panel-htop"]').find($('div[class="datagrid-view"]')).height(boxHeight-50);
				 
			}else{
				$('.tab-content div[class="panel panel-default datagrid panel-htop"]').eq(2).height(272);
				$('#proTestcase').prev().find($('div[class="datagrid-body"]')).height(190);
				$('.tab-content div[class="panel panel-default datagrid panel-htop"]').eq(2).find($('div[class="datagrid-view"]')).height(230);
			}
			/*$('.tab-content div[class="panel panel-default datagrid panel-htop"]').eq(2).height(370);
			$('#proTestcase').prev().find($('div[class="datagrid-body"]')).height(282);
			$('.tab-content div[class="panel panel-default datagrid panel-htop"]').eq(2).find($('div[class="datagrid-view"]')).height(320);
		*/}
	});
}

//"操作"列
/*function operatFormat(value,row,index){
	var columnStr = "<div>" +
                     "<button type='button' class='btn btn-default' id='editRoleInfBtn' onclick='viewTestCase(\""+ row.packageId + "\")' >" +
                     "查看用例" +
                     "</button>"+
		             "</div>";
	return columnStr;
}*/

//关闭弹窗
$('#closeTestCaseWin').click(function(){
	objs.$proTestcase.xdatagrid('reload');
//	objs.$proTestcase.xdatagrid('load', {
//		 'dto.iterationList.iterationId': iterationId
//	});
    $('#iterationTestCaseLayout').xdialog('destroy');
});

function tip(info){
	$.xconfirm({
		msg:info,
		okFn: function() {
			
		},
		cancelFn: function(){
			
		}
	});
}

//加载可分配人列表
function loadExecPeopleLists(){
	$.post(
			baseUrl + "/otherMission/otherMissionAction!getPeopleLists.action",
			null,
			function(dat) {
				if (dat != null) {
					peopleList = dat.rows;
	
					var opti = '<option value="all" selected="selected">-全部人员-</option>';
					if(dat.rows.length > 0){
						for(var i=0;i<dat.rows.length;i++){	
								opti = opti + '<option value="'+dat.rows[i].id+'">'+dat.rows[i].name+'</option>';
						}
					}
					$("#execPeopleNm").html(opti);
					$("#execPeopleNm").off().on("change",function(){
						var queryParam = document.getElementById("testCasepkgNm").value;
				    	var peopleId = document.getElementById("execPeopleNm").value;
				    	var dataParam = {
				    			'dto.testCasePackage.taskId':taskId,
				    			'dto.selectedUserIds':peopleId,
				    			'dto.packageIdJoin':packageIds
				    	}
				    	if(queryParam){
				    		dataParam['dto.queryParam']=queryParam
				    	}
				    	//模糊查询
				    	$("#TestCaseList").xdatagrid('reload',dataParam);
					});
					$('#execPeopleNm').searchableSelect();
				} else {
					$.xalert("系统错误！", {type:'warning'});
				}
			}, "json");
}

document.getElementById('toolWrapper').onkeydown=function(event) {
	var e = event || window.event || arguments.callee.caller.arguments[0]; 
    if (e && e.keyCode == 13) {//keyCode=13是回车键；数字不同代表监听的按键不同
    	var queryParam = document.getElementById("testCasepkgNm").value;
    	var peopleId = document.getElementById("execPeopleNm").value;
    	var dataParam = {
    			'dto.testCasePackage.taskId':taskId,
    			'dto.selectedUserIds':peopleId,
    			'dto.packageIdJoin':packageIds
    	}
    	if(queryParam){
    		dataParam['dto.queryParam']=queryParam
    	}
    	//模糊查询
    	$("#TestCaseList").xdatagrid('reload',dataParam);
    }
};

//# sourceURL=iterationTestCaseLayout.js